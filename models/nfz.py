
import sys
if __name__ != "__main__":
  sys.path.append("models/")
from hl_global import boundary, timeframe
import models.utils as utils


# dont forget
# nfzVersrion
# lastUpdate unix timesamp + ISO
# openapi_nfz()
class nfz:

    def __init__(self) -> None:
        self.id = "NA"
        self.name = "NA"
        self.altitude = 0.0
        self.boundary = boundary()
        self.timeframe = timeframe()
        # self.airVehicleId = "abc"
        self.dsosId = None
        self.dsosIds = None
        self.excludedDsosIds = []
        self.tags = []
        self.type = ""
        self.dnfz_speed = 0
        self.dnfz_is_active = False
        self.dnfz_path = boundary() #TODO just asuming this type
        self.ctr = False
        self.container = False
        self.corridor = False

        self.radius = 100
        self.c_lat = 0
        self.c_lon = 0
        
    
    def get_awz_coords(self):
        pois = []
        
        tmp_list = self.boundary.get_lat_lon()
        for pos in tmp_list:
            tmp = {}

            if len(pos) == 2: 
                tmp["altitude"] = 0  
            else:
                 tmp["altitude"] = pos[2]

            tmp["latitude"] = pos["latitude"]
            tmp["longitude"] = pos["longitude"]
            tmp["id"] = hash(str(pos["latitude"]))
            tmp["nfz_id"] = self.id
            
            pois.append(tmp)

        return pois


    def get_awz_dict(self):

        dic = {}
        dic["id"] = self.id
        dic["name"] = self.name
        
        dic["timestamp"] = self.timeframe.startDate
        dic["expiration"] = self.timeframe.endDate
        dic["ctr"] = {}
        dic["height"] = self.altitude
        if self.type == "circle":
            dic["type"] = self.type
            dic['circle'] = {"radius": self.radius, "center":  {"latitude": self.c_lat, "longitude": self.c_lon, "altitude": 30, "id": self.id}}
            
        else:
            dic["polygon"] = self.get_awz_coords()
            dic["type"] = "polygon"
        # dic["radius"] = self.radius
        # dic["center"] = {"latitude": self.c_lat, "longitude": self.c_lon, "altitude": 30}

        if self.ctr == True:
            dic["ctr"] = {"org_id": self.dsosId, "id":self.id}

        if self.container == True:
            dic["container"] = {"org_id": self.dsosId}

        return dic

    def init_from_hl_container(self, init_dic, dsosId="NA"):
        self.init_from_hl_nfz(init_dic)
        self.container = True
        self.dsosId = dsosId
        return self

    def init_from_hl_dynamic_nfz(self, init_dic):
        self.init_from_hl_nfz(init_dic)
        return self


    def init_from_hl_ctr(self, init_dic, air_dsos_id="NA"):
        self.init_from_hl_nfz(init_dic)
        self.ctr = True
        self.dsosId = air_dsos_id

        return self
    

    def init_from_hl_corridor(self, init_dic, drone_id):
        self.init_from_hl_nfz(init_dic)
        self.corridor = True
        self.name = str(drone_id) + " FlightPlanBoundry"
        
        return self


    def init_from_hl_nfz(self, init_dic):
        self.id = init_dic["id"]
        self.name = init_dic["name"]
        if len(init_dic["boundary"][0]["polygon"]) == 65:
            self.type = "circle"
            
            self.c_lat = init_dic["centerPoint"]["coordinates"]["latitude"]
            self.c_lon = init_dic["centerPoint"]["coordinates"]["longitude"]
            point_on_circle_lat = init_dic["boundary"][0]["polygon"][0]["coordinates"]["latitude"]
            point_on_circle_lon = init_dic["boundary"][0]["polygon"][0]["coordinates"]["longitude"]
            distance = utils.AzLatLng.calculateDistanceInMeters(self.c_lat,  self.c_lon, point_on_circle_lat, point_on_circle_lon)

            self.center = distance

        elif "type" in init_dic:
            self.type = init_dic["type"]

        # if self.name != "111":
        self.boundary.init_from_dict(init_dic["boundary"][0])
        self.ctr = False
        

        if "dsosIds" in init_dic:
            self.dsosIds = init_dic["dsosIds"]

        return self
from enum import Enum
from datetime import datetime, timedelta
from http.client import TEMPORARY_REDIRECT
from os import stat

from matplotlib.pyplot import polar
import numpy
from urllib3 import Retry

UTC_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'

class FLIGHT_MODES(Enum):
    MANUAL = "manual"
    CONTROLLED = "controlled"


class FLIGHT_STATUS(Enum):
    TAKEOFF = "takeoff"
    ONROUTE = "onroute"
    LAND	 = "land"
    RTH   = "rth"
    HOVER = "hover"
    NOTACTIVE = "notactive"

    @staticmethod
    def get_awz_teminology(ft):
        if isinstance(ft, str):
            if ft == "takeoff": return "102"
            if ft == "land": return "104"
            if ft == "notactive": return "101"
            if ft == "hover" or "rth" or "onroute": return "103"

        return "103"




class GEOFENCE_TYPE(Enum):
    CTR = "ctr"


class FLIGHT_PLAN_STATUS(Enum):
    PENDING = "pending"
    DECLINED = "declined"
    APPROVED = "approved"
    EXECUTING = "executing"
    DONE = "done"
    PASSED = "passed"
    CANCELLED = "cancelled"
    NOTACTIVE = "notactive"


class FLIGHT_PLAN_TYPE(Enum):
    MAPPING = "mapping"
    AGRICULTURE = "agriculture"
    DELIVERY = "delivery"
    SECURITY = "security"
    FIRST_RESPONSE = "first response"
    INSPECTION = "inspection"
    PHOTOGRAPHY = "photography"
    EMERGENCY = "emergency"
    HOVER = "hover"
    RTH = "rth"
    DTM = "dtm"


class FLIGHT_PLAN_TERMINATION_REASON(Enum):
    FINISHED = "finished"
    CANCEL = "cancel"
    RTH = "rth"
    SHUT_DOWN = "shut_down"


class AIR_SITUATION_TYPE(Enum):
    FULL = "full"
    STATIC = "static"
    DYNAMIC = "dynamic"
    NONE = "none"


class ALERTS_TYPE(Enum):
    FULL = "full"
    UAV = "uav"


class ERROR_STATUS_MESSAGE(Enum):
    COLLISION = "collision"
    VALIDATION = "validation"
    SYSTEM = "system"
    SUGGESTION = "suggestion"
    AUTHENTICATION = "authentication"


class timeframe:
    def __init__(self, end_time_sec=None) -> None:
        self.startDate = datetime.utcnow().strftime(UTC_FORMAT)
        if end_time_sec is not None:
            self.endDate = (datetime.utcnow() + timedelta(seconds=end_time_sec)).strftime(UTC_FORMAT)
        else:
            self.endDate = (datetime.utcnow() + timedelta(days=20.0)).strftime(UTC_FORMAT)
    
    # def init_from_hl(self, init_dict):
    #     self.startDate = datetime.str(init_dict["startDate"]) 
    #     # self.endDate = datetime.str(init_dict["endDate"]) 


    def to_dict(self):

        return {"startDate": self.startDate, "endDate": self.endDate}

    def to_dict_short(self, newtime=None):
        if newtime is not None:
            self.startDate = newtime
        return {"startDate": self.startDate}

class hl_coords:
    def __init__(self, lat=3.0, lon=3.0) -> None:
        self.latitude = lat
        self.longitude = lon

    def to_dict(self):
        return self.__dict__

    def init_from_dict(self, dic):
        self.latitude = dic["latitude"]
        self.longitude = dic["longitude"]

        return self



class hl_position:
    def __init__(self, lat=3.0, lon=3.0, agl=0.0, asl=0.0) -> None:
        self.coordinates = hl_coords(lat, lon)
        self.AGL = agl
        self.ASL = asl

    def init_from_dict(self, dic):
        self.AGL = dic.get("AGL", 0)
        self.ASL = dic.get("ASL", 0)
        if "coordinates" in dic:
            self.coordinates = hl_coords().init_from_dict(dic["coordinates"])
        else:
            self.coordinates = hl_coords().init_from_dict(dic)


        

        return self

    def to_dict(self):
        dic = self.__dict__
        if hasattr(self.coordinates, "to_dict"):
            dic["coordinates"] = self.coordinates.to_dict()
        elif isinstance(self.coordinates, dict):
            dic["coordinates"] = self.coordinates
        else:
            dic["coordinates"] = {}
        return dic

    def get_lat_lon(self):
        return self.coordinates.to_dict()

    def get_awz_dict(self):
        dic = dict()
        dic["latitude"] = self.coordinates.latitude
        dic["longitude"] = self.coordinates.longitude
        dic["altitude"] = self.ASL
        return dic

class boundary:

    def __init__(self) -> None:
        self.polygon = []

    def to_dict(self):
        tmp_dic = self.__dict__
        for i in range(len(tmp_dic["polygon"])):
            if hasattr(tmp_dic["polygon"][i], "to_dict"):
                tmp_dic["polygon"][i] = tmp_dic["polygon"][i].to_dict()
        
        return tmp_dic

    def init_from_dict(self, init_dict):

        if len(init_dict["polygon"]) == 65:# circle
            return self

        for pos in init_dict["polygon"]:
            asl = pos["ASL"] if pos["ASL"] is not None else 0
            agl = pos["AGL"] if pos["AGL"] is not None else 0
            lat = pos["coordinates"]["latitude"]
            lon = pos["coordinates"]["longitude"]
            self.polygon.append(hl_position(lat, lon, agl, asl))

        return self

    def get_lat_lon(self):
        tmp_list = []
        for poi in self.polygon:
            if isinstance(poi, hl_position):
                tmp_list.append(poi.get_lat_lon())
        return tmp_list



class hl_velocity:
    def __init__(self, dic=None) -> None:
        if dic is None:
            self.x = 0.0
            self.y = 0.0
            self.z = 0.0
        else:
            self.x = dic.get('x', 0.0)
            self.y = dic.get('y', 0.0)
            self.z = dic.get('z', 0.0)


    def get_total(self):
        s = numpy.array([self.x, self.y, self.z])
        return numpy.linalg.norm(s)


    def to_dict(self):
        return self.__dict__


class container:
    def __init__(self) -> None:
        self.id = 0.0
        self.boundary = 0.0
        self.timeFrame = 0.0
        self.name = ""

    def export_awz_style_obj(self):
        pass


class bridge:
    def __init__(self) -> None:
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0

    def export_awz_style_obj(self):
        pass


class flightplan:
    def __init__(self) -> None:
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0

    def export_awz_style_obj(self):
        pass

if __name__ == "__main__":
    #  usage
    # print(FLIGHT_MODES("manual"))
    # b = boundary()
    # b.polygon.append(hl_position())
    # b.polygon.append(hl_position())
    # b.polygon.append(hl_position())
    # print(b.get_lat_lon())
    pass


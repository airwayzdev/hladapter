


from datetime import datetime
import traceback

from models.hl_global import UTC_FORMAT


alert_type = [""]
"""
{'code': 'approved', 
'entity': {'id': 'AdminDrone 3', 'name': 'AdminDrone 3', 'model': 'unknown', 'owner': '0'}, 
'path': {'startTime': '2022-02-28T15:26:47.118Z', 
'wp': [
    {'altitude': 30, 'latitude': 32.430996675121726, 'longitude': 35.03239869874035, 'name': '', 'id': -1, 'speed': 5, 'accuracy': 0.0, 'delay': 4, 'required': True}, 
    {'altitude': 30, 'latitude': 32.43101039459844, 'longitude': 35.02816028192675, 'name': '', 'id': -1, 'speed': 5, 'accuracy': 0.0, 'delay': 4, 'required': True}, 
    {'altitude': 30, 'latitude': 32.435175848222585, 'longitude': 35.030906863958, 'name': '', 'id': -1, 'speed': 5, 'accuracy': 0.0, 'delay': 4, 'required': True}, 
    {'altitude': 30, 'latitude': 32.43586403508108, 'longitude': 35.025070377141596, 'name': '', 'id': -1, 'speed': 5, 'accuracy': 0.0, 'delay': 4, 'required': True}]},
     'sugpath': None, 'nfz': None, 'collisions': []}

     {'code': 'reject_collision', 
     'entity': {'id': 'qa1dr1', 'name': 'qa1dr1', 'model': 'QA_Drone', 'owner': ''}, 
     'path': {'startTime': '2022-02-27T13:42:03.739Z', 
     'wp': [{'altitude': 20, 'latitude': 32.390133999999996, 'longitude': 34.9562543, 'name': 'droneLocation', 'id': -1, 'speed': 5, 'accuracy': 0, 'delay': 0, 'required': True}, 
     {'altitude': 20, 'latitude': 32.387133999999996, 'longitude': 34.9562543, 'name': 't1', 'id': 0, 'speed': 5, 'accuracy': 2, 'delay': 4, 'required': True}, 
     {'altitude': 20, 'latitude': 32.384133999999996, 'longitude': 34.9562543, 'name': 't2', 'id': 1, 'speed': 5, 'accuracy': 2, 'delay': 4, 'required': True}, 
     {'altitude': 20, 'latitude': 32.381133999999996, 'longitude': 34.9562543, 'name': 't3', 'id': 2, 'speed': 5, 'accuracy': 2, 'delay': 4, 'required': True}]}, 
     'sugpath': None, 
     'nfz': None, 
     'collisions': [
         {'pid': 'qa1dr5', 'name': 'qa1dr5', 'owner': '', 'minDistance': 0.0, 
         'position': {'altitude': 20.0, 'latitude': 32.387133999999996, 'longitude': 34.9562543}}]}
"""

awz_alert_types = ["reject_collision", "reject_nfz", "reject_system"]
class alert:

    def __init__(self) -> None:
        self.name = "NA"
        self.code = "reject_system"
        self.sugpath = None
        self.nfz = None
        self.collisions = []
        self.wp = []
        self.entity =  {'id': 'NA', 'name': 'NA', 'model': 'NA', 'owner': '0'}
        # Dummut path
        self.path = {'startTime': '2022-02-27T13:42:03.739Z', 
     'wp': [{'altitude': 20, 'latitude': 32.390, 'longitude': 34.95, 'name': '', 'id': -1, 'speed': 5, 'accuracy': 0, 'delay': 0, 'required': True}, 
     {'altitude': 20, 'latitude': 32.3871, 'longitude': 34.95, 'name': 't1', 'id': 0, 'speed': 5, 'accuracy': 2, 'delay': 4, 'required': True}]}
        self.sug_startTime = None


    def init_from_hl_alert(self, init_dict):
        self.entity["id"] = init_dict["entity"]["name"]
        self.entity["name"] = init_dict["entity"]["name"]
        speed = init_dict["entity"]["speed"]
        self.entity["model"] = ""
        self.entity["owner"] = ""
        if "statusMessage" in init_dict:
            # if init_dict['suggestion']: # there is a sugg
            err_type = init_dict["errors"][0]
            if 'nfz' in err_type or "ctr" in err_type:
                self.code = awz_alert_types[1]
                if "nfz" in err_type:
                    obj = err_type["nfz"][0]["name"]
                elif "ctr" in err_type:
                    obj = err_type["ctr"][0]["name"]
                elif "flight_plan" in err_type:
                    obj = err_type["flight_plan"][0]["name"]

                self.nfz = {"id": obj}
                self.collisions = []
            elif 'telemetries' in err_type or "flight_plan" in err_type or "dynamic_nfz" in err_type or "err_type" in err_type:
                self.code = awz_alert_types[0]
                name = err_type.get("flight_plan", {"name": "NA"}) #[0]["name"]
                if isinstance(name, dict):
                    name = err_type.get("flight_plan", {"name": "NA"})["name"]
                elif isinstance(name, list):
                    name = err_type.get("flight_plan", {"name": "NA"})[0]["name"]
                else:
                    name = err_type.get("flight_plan", {"name": "NA"})[0]["name"]
                self.collisions = [ {'pid': name, 'name': name, 'owner': '', 'minDistance': 0.0, 
                'position': {'altitude': 20.0, 'latitude': 32.387133999999996, 'longitude': 34.9562543}}]
            else:
                print(f"ERORR: No handeled error {err_type}")
            if "suggestions" in init_dict:
                sugg_list = init_dict["suggestions"]["flightPlan"]["checkpoints"]

                self.sugpath = []
                # Insert first dummy object (its discarted by the uss)
                self.sugpath.append({'altitude': 20, 'latitude': 32.390, 'longitude': 34.95, 'name': 'T0', 'id': -1, 'speed': 5, 'accuracy': 0, 'delay': 0, 'required': True})
                poi_number = 1
                for item in sugg_list:
                    alt = item["ASL"]
                    lat = item["coordinates"]["latitude"]
                    lon = item["coordinates"]["longitude"]
                    obj = {'altitude': alt, 'latitude': lat, 'longitude': lon, 'name': 'T'+str(poi_number), 'id': -1, 'speed': speed, 'accuracy': 0, 'delay': 0, 'required': True}
                    poi_number += 1
                    self.sugpath.append(obj)
                self.sug_startTime = datetime.utcnow().strftime(UTC_FORMAT)
            else:
                self.sugpath = None
                self.sug_startTime = None
                
        else:
            print("Error in Alert initialition")

        return self

    def init_from_hl_uav_alert(self, init_dict):
        try:
            self.name = init_dict["tailNumber"]
            self.id = init_dict["tailNumber"]
            sugg_list = init_dict["data"]["flightPlan"]["checkpoints"]

            self.sugpath = []
            # Insert first dummy object (its discarted by the uss)
            self.sugpath.append({'altitude': 20, 'latitude': 32.390, 'longitude': 34.95, 'name': 'T0', 'id': -1, 'speed': 5, 'accuracy': 0, 'delay': 0, 'required': True})
            poi_number = 1
            for item in sugg_list:
                alt = item["ASL"]
                lat = item["coordinates"]["latitude"]
                lon = item["coordinates"]["longitude"]
                obj = {'altitude': alt, 'latitude': lat, 'longitude': lon, 'name': 'T'+str(poi_number), 'id': -1, 'speed': 8, 'accuracy': 0, 'delay': 0, 'required': True}
                poi_number += 1
                self.sugpath.append(obj)
            self.sug_startTime = datetime.utcnow().strftime(UTC_FORMAT)
        except Exception as ex:
            print(ex, f"\ninput: {init_dict}")
        return self

    def get_awz_sync_alert(self):
        
        dic = {}
        dic["id"] = 2
        dic["time"] = self.sug_startTime
        dic["type"] = ''
        dic["source"] = {"id": "Hilander Adapter", "name": "Hilander Adapter"}
        dic["drones"] = []
        if len(self.sugpath) > 0:
            tmp_drone = {}
            tmp_drone["id"] = self.name
            tmp_drone["mindistance"] = 0.1
            tmp_drone["owner"] = {"id": 3418296952414208, "name": "", "url": ""}
            tmp_drone["owner"]["position"] = {"latitude": 32.428, "longitude": 34.965, "altitude": 32.42}
            tmp_drone["sugpath"] = self.sugpath
            dic["drones"].append(tmp_drone)


        dicdic = {"collisions": [dic], "drones": [], "global": []}
        return dicdic

    def get_awz_alert(self):
        res = {'code': self.code, 
     'entity': self.entity, 
     'path': {'startTime': '2022-02-27T13:42:03.739Z', 
     'wp': [{'altitude': 20, 'latitude': 32.390133999999996, 'longitude': 34.9562543, 'name': 'droneLocation', 'id': -1, 'speed': 5, 'accuracy': 0, 'delay': 0, 'required': True}, 
     {'altitude': 20, 'latitude': 32.387133999999996, 'longitude': 34.9562543, 'name': 't1', 'id': 0, 'speed': 5, 'accuracy': 2, 'delay': 4, 'required': True}]}, 
     'sugpath': {"startTime": self.sug_startTime, "wp": self.sugpath}, 
     'nfz': self.nfz, 
     'collisions': self.collisions}
        return res
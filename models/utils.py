import numpy as np
from math import fabs, sqrt, acos, cos, sin, pi, atan2, asin

R = 6371000
class AzGpsPosition:

    def __init__(self, dict_data):
        self.lat = dict_data.get("latitude", 0.0)
        self.lng = dict_data.get("longitude", 0.0)
        self.alt = dict_data.get("altitude", 0.0)
        self.time = dict_data.get("time", 0)


    def get_dict(self):
        return {"latitude": self.lat, "longitude": self.lng, "altitude": self.alt, "time": self.time}


    def init_from_dict(self, dict_data):
        if type(dict_data) is dict:
            self.lat = float(dict_data["latitude"])
            self.lng = float(dict_data["longitude"])
            self.alt = float(dict_data["altitude"])
            self.time = dict_data["time"]
        elif type(dict_data) is AzGpsPosition:
            return self.set_from(dict_data)

        return self

    def set_from(self, gpsPosition):
        self.lat = float(gpsPosition.lat)
        self.lng = float(gpsPosition.lng)
        self.alt = float(gpsPosition.alt)
        self.time = gpsPosition.time
        return self
    

    def bearingRad(self, gpsPosition):
        return AzLatLng.calculateBearingRadians(self.lat, self.lng, gpsPosition.lat, gpsPosition.lng)

    def distance(self, gpsPosition):
        return AzLatLng.calculateDistanceInMeters(self.lat, self.lng, gpsPosition.lat, gpsPosition.lng)

    def position(self, distance, bearing):
        latlng = AzLatLng.calculatePosition(self.lat, self.lng, distance, bearing)
        return (latlng.lat, latlng.lng, self.alt, self.time)

class AzLatLng:

    def __init__(self, lat, lng, alt=0):
        self.lat = lat
        self.lng = lng
        self.alt = alt


    @staticmethod
    def deg2rad(deg):
        return deg*(pi/180)

    @staticmethod
    def rad2deg(rad):
        return rad*(180/pi)
    
    @staticmethod
    def calculateDistanceInMeters(lat1, lng1, lat2, lng2):
        
        φ1 = AzLatLng.deg2rad(lat1)
        lng1 = AzLatLng.deg2rad(lng1)
        φ2 = AzLatLng.deg2rad(lat2)
        lng2 = AzLatLng.deg2rad(lng2)

        Δφ = (φ1-φ2)
        Δλ = (lng2-lng1)

        a = sin(Δφ/2) * sin(Δφ/2) + cos(φ1) * cos(φ2) * sin(Δλ/2) * sin(Δλ/2)
        c = 2 * atan2(sqrt(a), sqrt(1-a))

        d = R * c
        return d

    @staticmethod
    def calculateBearingRadians(lat1, lng1, lat2, lng2):

        φ1 = AzLatLng.deg2rad(lat1)
        λ1 = AzLatLng.deg2rad(lng1)
        
        φ2 = AzLatLng.deg2rad(lat2)
        λ2 = AzLatLng.deg2rad(lng2)
        
        y = sin(λ2-λ1) * cos(φ2)
        x = cos(φ1)*sin(φ2) -sin(φ1)*cos(φ2)*cos(λ2-λ1)
        brng = atan2(y, x)
       
        return brng

    @staticmethod
    def calculatePosition(lat, lng, distance, bearing):

        φ1 = AzLatLng.deg2rad(lat)
        λ1 = AzLatLng.deg2rad(lng)
        d = distance
        bearing = AzLatLng.deg2rad(bearing)

        φ2 = asin( sin(φ1)*cos(d/R) + cos(φ1)*sin(d/R)*cos(bearing) )
        λ2 = λ1 + atan2(sin(bearing)*sin(d/R)*cos(φ1), cos(d/R)-sin(φ1)*sin(φ2))

        return AzLatLng(AzLatLng.rad2deg(φ2) ,AzLatLng.rad2deg(λ2))

    def distance(self, lat, lng):
        return AzLatLng.calculateDistanceInMeters(self.lat, self.lng, lat, lng)

    def bearing(self, lat, lng):
        return AzLatLng.calculateBearingRadians(self.lat, self.lng, lat, lng)

    def position(self, distance, bearing):
        return AzLatLng.calculatePosition(self.lat, self.lng, distance, bearing)

def calculate_flightplan_duration_and_max_height_and_speed(path, takeofalt=0, mergin=0) -> int:
    t = 0
    h = 0
    s = 0
    try:
        for i in range(1, len(path)):
            poi1, poi2 = path[i-1], path[i]
            lat1, lon1 = poi1["latitude"], poi1["longitude"] 
            lat2, lon2 = poi2["latitude"], poi2["longitude"]
            delay = poi2.get("delay", 0)
            speed = poi2["speed"]
            s += speed
            distance = AzLatLng.calculateDistanceInMeters(lat1, lon1, lat2, lon2)
            if h < poi1["altitude"]: 
                h = poi1["altitude"]
            t += distance/speed + delay
    except:
        t = 180
        h = 300

    return int(t), h + takeofalt + mergin, s/len(path)



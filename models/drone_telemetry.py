
from random import random
import time
import sys
import traceback
from enum import Enum

from matplotlib.pyplot import flag
from numpy import square

from models.hl_global import FLIGHT_MODES, FLIGHT_STATUS, UTC_FORMAT, hl_position as hl_position_class, hl_velocity as hl_velocity_class
if __name__ != "__main__":
  sys.path.append("models/")
from math import pi
from datetime import datetime, timedelta
from hl_global import boundary, timeframe, FLIGHT_PLAN_TYPE


AIRWAYZ_DSOS_ID = ""
# class droneTypeE(Enum):
#     """
#     "enum": [
#         "dji",
#         "alpha",
#         "pixhawk"
#       ],

#     """
#     dji = "dji"
#     alpha = "alpha"
#     pixhawk = "pixhawk"

# flightStatus_dict = {
#       "101": "NotActive",
#       "102": "TakeOff",
#       "103": "OnRoute",
#       "105": "RH",
#       "104": "Land",
#       "NotActive": "101",
#       "TakeOff": "102",
#       "OnRoute": "103",
#       "RH": "105",
#       "Land": "104"
#     }

# class flightStatus(Enum):
#     '''
#     "enum": [
#         "NotActive",
#         "TakeOff",
#         "OnRoute",
#         "RH",
#         "Land",
#         "Hover"
#       ]
#     '''
#     NotActive = "NotActive" # 101
#     TakeOff = "TakeOff" # 102
#     OnRoute = "OnRoute" # 103
#     RH = "RH" # 105
#     Land = "Land" # 104
#     Hover = "Hover"
    
    


# class flightModeE(Enum):
#     """
#     "enum": [
#         "Manual",
#         "GCS",
#         "Auto",
#         "EmergencyGCS",
#         "EmergencyAuto"
#       ],
#     """
#     Manual = "Manual"
#     GCS = "GCS"
#     Auto = "Auto"
#     EmergencyGCS = "EmergencyGCS"
#     EmergencyAuto = "EmergencyAuto"
    

# class GPS_StatusE(Enum):
#     """
#     "enum": [
#         "FIX"
#         "NotAccurate"
#         "NoGps"
#       ],
#     """
#     FIX = "FIX"
#     NotAccurate = "NotAccurate"
#     NoGps = "NoGps"
class IHReqFlightPlan:
  name : str
  radiusInMeters : float
  checkpoints : list # path points
  type : FLIGHT_PLAN_TYPE
  tailNumber : str 
  serialNumber : str
  timeFrame : timeframe
  shouldClearTrail : bool
  speed : float

  def __init__(self, init_dict=None) -> None:
    self.name = "NA"
    self.radiusInMeters = 30.0
    self.checkpoints = []
    self.type = FLIGHT_PLAN_TYPE.DELIVERY.value
    self.tailNumber = "NA"
    self.serialNumber = "NA"
    self.timeFrame = timeframe().to_dict()
    self.shouldClearTrail = False
    self.speed = 5.0
    if init_dict is not None and isinstance(init_dict, dict):
      self.name = init_dict.get("name", "NA")
      self.radiusInMeters = init_dict.get("radiusInMeters", 30.0)
      self.checkpoints = init_dict.get("checkpoints", [])
      self.type = FLIGHT_PLAN_TYPE.DELIVERY.value
      self.tailNumber = init_dict.get("tailNumber", "NA")
      self.serialNumber = init_dict.get("serialNumber", "NA")
      duration_in_second = init_dict.get("duration_in_second", 3600.0)
      self.timeFrame = timeframe(duration_in_second).to_dict()
      self.speed = init_dict.get("speed", 5.0)
      self.shouldClearTrail = True


  def to_dict(self):
    return self.__dict__

class IHReqTelemetry:
  def __init__(self) -> None:
      self.type = "delivery"
      self.tailNumber = "delivery"
      self.serialNumber = "delivery"
      self.battery = "delivery"
      self.heading = "delivery"
      self.takeoffLocation = "delivery"
      self.isFlying = True
      self.flightStatus = FLIGHT_STATUS
      self.roll = 0.0
      self.pitch = 0.0
      self.yaw = 0.0
      self.satellite = 0.0
      self.velocity = 0.0
      self.position = 0.0
      self.flightMode = 0.0
      self.timestamp = 0.0
      self.fpId = 0.0


class droneTelemetry:
    hl_id : str # id of telemetry
    hl_type : FLIGHT_PLAN_TYPE # drone mission description
    hl_tailNumber : str # Tail number of the UAV. This is a unique value as registered and recieved by the Airspace authorities.
    hl_serialNumber : str # Serial number of the UAV
    hl_model : str
    hl_battery : float
    hl_heading : float # in degrees
    hl_takeoffLocation : hl_position_class
    hl_isFlying : bool # isFlying
    hl_flightStatus : FLIGHT_STATUS
    hl_pitch : float
    hl_roll : float
    hl_yaw : float
    hl_satellite : float
    hl_velocity : hl_velocity_class
    hl_position : hl_position_class
    hl_flightMode : FLIGHT_MODES # manual or controlled
    hl_timestamp : str
    hl_fpid : str


    # Airwayz objects
    airwayz_drone_id_number : int
    airwyaz_drone_id_in_data_drone : int
    airwayz_model : str
    airwayz_is_controlable : bool
    airwyaz_description : list
    airwayz_attitude : dict
    airwayz_path : list


    def init_from_hl_uav_telemtry(self, hl_uav_obj):
        self.dsosId = "hl"
        self.hl_id = hl_uav_obj["id"]
        self.hl_type = FLIGHT_PLAN_TYPE.INSPECTION.value 
        self.hl_tailNumber = hl_uav_obj["tailNumber"]
        self.hl_serialNumber = hl_uav_obj.get("serialNumber", "no_serialnumber")
        self.hl_battery = hl_uav_obj["battery"]
        self.hl_heading = hl_uav_obj["yaw"]
        if "takeoffLocation" in hl_uav_obj and hl_uav_obj["takeoffLocation"] is not None:
          self.hl_takeoffLocation = hl_position_class().init_from_dict(hl_uav_obj["takeoffLocation"])
        else:
          self.hl_takeoffLocation = hl_position_class()
          
        self.hl_isFlying = hl_uav_obj.get("isFlying", True)
        self.hl_model = hl_uav_obj["model"]
        #FIXME arbitrary value!
        self.hl_flightStatus = FLIGHT_STATUS.ONROUTE.value
        self.hl_pitch = hl_uav_obj["pitch"]
        self.hl_roll = hl_uav_obj["roll"]
        self.hl_yaw = hl_uav_obj["yaw"]
        self.hl_satellite = hl_uav_obj.get("satellite", 10.0)
        if "velocity" in hl_uav_obj:
          self.hl_velocity = hl_velocity_class(hl_uav_obj["velocity"])
        else:
          self.hl_velocity = hl_velocity_class()

        self.hl_position = hl_position_class().init_from_dict(hl_uav_obj["position"])
        
        self.hl_flightMode = FLIGHT_MODES.CONTROLLED.value
        self.hl_timestamp = hl_uav_obj["timestamp"]
        self.hl_fpid = hl_uav_obj.get("fpId", "NA")

        self.airwayz_model = "HighLander Drone"
        self.airwayz_attitude = {"yaw" : self.hl_heading*(pi/180), "roll": 0, "pitch": 0, "time" : int(time.time())}
        self.airwyaz_description = ["unmanned"]
        self.airwayz_drone_id_number = int(random()*100000)#int(self.hl_serialNumber)
        self.airwyaz_drone_id_in_data_drone = int(random()*100000)#int(self.hl_serialNumber)
        self.airwayz_is_controlable = True
        self.airwayz_path = []

        return self

    
    def get_hl_uav_telemtry(self):
      dic = dict()
      
      dic["id"] = self.hl_id
      dic["type"] = self.hl_type
      dic["tailNumber"] = self.hl_tailNumber
      dic["serialNumber"] = self.hl_serialNumber
      dic["battery"] = self.hl_battery
      dic["model"] = self.hl_model
      dic["heading"] = self.hl_heading
      dic["takeoffLocation"] = self.hl_takeoffLocation.to_dict()
      dic["isFlying"] = self.hl_isFlying
      dic["flightStatus"] = self.hl_flightStatus
      dic["pitch"] = self.hl_pitch
      dic["roll"] = self.hl_roll
      dic["yaw"] = self.hl_yaw
      dic["satellite"] = self.hl_satellite
      dic["velocity"] = self.hl_velocity.to_dict()
      dic["position"] = self.hl_position.to_dict()
      dic["flightMode"] = self.hl_flightMode
      dic["timestamp"] = self.hl_timestamp
      dic["fpId"] = self.hl_fpid

      return dic

    def get_awz_uav_telemtry(self):
      """
      should be in a format: 
      {
        "usses" : ...,
        "nfz" : ...
      }

      """
      # here the forma of usses dict
      entety = {}
      try:
        drone = {}
        pilot_position = self.hl_takeoffLocation.get_awz_dict()
        pilot_position["time"] = int(time.time())
        pilot = {"id": "pilot_"+self.hl_id,
                "callname": "cn_"+self.hl_id,
                "position":  pilot_position}

        drone["attitude"] = self.airwayz_attitude
        drone["battery"] = float(int(self.hl_battery) / 100)
        drone["descriptors"] = ["unmanned"]
        # drone["isManned"] = False
        drone["flyingState"] = FLIGHT_STATUS.get_awz_teminology(self.hl_flightStatus)
        drone["id"] = self.airwayz_drone_id_number
        drone["isControllable"] = True

        drone["model"] = self.airwayz_model
        drone["name"] = self.hl_id

        # path #TODO what to do with the nextPOI???
        # poi = self.route.awz_get(speed=self.velocity, default_height=self.altASL)
        drone["path"] = {"nextPoiIndex": -1, "poi" : []}

        # positions #TODO need to calculate where the drone was
        cur_location_dict = self.hl_position.get_awz_dict()
        cur_location_dict["time"] = int(time.time()*1000)
        cur_location_dict["alt_isa"] = 120.63
        cur_location_dict["altitude_datum"] = "wgs84"
        p1 = cur_location_dict # past
        p1["time"] -= 1000
        p2 = cur_location_dict # present
        drone["positions"] = [p1, p2]
        
        drone["velocity"] = {"vx": self.hl_velocity.x, "vy": self.hl_velocity.y, "vz": self.hl_velocity.z, "v_total":self.hl_velocity.get_total()}

        entety["chat"] = {}
        entety["data"] = {"pilot" : pilot, "drone" : drone, "alerts" : {"collisions" : []}, "appVersion": "Airwayz-HighLander-Adapter"}
        entety["id"] = self.airwayz_drone_id_number

      except Exception as ex:
        print(ex, traceback.format_exc())
        return None

      return entety


    def init_from_awz(self,awz_dsos_id_org_id="AZ", key_drone_name="NA", awz_obj=None, fp_id=None):
        # should dic {time: {}, enteties: {}} -> enteties
        try:
          self.dsosId = awz_dsos_id_org_id
          data = awz_obj["data"]
          drone = data["drone"]
          
          identfication = key_drone_name
          # if drone["name"] != "":
          #   identfication = str(drone["name"])
          # elif drone["id"] != "":
          #   identfication = str(drone["id"])
  

          
          self.hl_id = identfication
          self.hl_type = FLIGHT_PLAN_TYPE.DELIVERY.value
          self.hl_tailNumber = identfication
          self.hl_model = identfication
          self.hl_serialNumber = identfication
          self.hl_battery = float(drone["battery"]*100)
          head_tmp = float(drone["attitude"]["yaw"])
          self.hl_heading = head_tmp * (180/pi)
          home_pos = data["pilot"]["position"]
          self.hl_takeoffLocation = hl_position_class(lat=home_pos["latitude"], lon=home_pos["longitude"], asl=home_pos["altitude"], agl=float(drone.get("takeoffAlt", 0)))
          self.hl_isFlying = ("101" != drone["flyingState"] and "100" != drone["flyingState"])
          flight_status_convertion = {"102": FLIGHT_STATUS.TAKEOFF.value, "104": FLIGHT_STATUS.LAND.value}
          flight_status_convertion["100"] = FLIGHT_STATUS.NOTACTIVE.value
          flight_status_convertion["101"] = FLIGHT_STATUS.NOTACTIVE.value
          if drone["flyingState"] in flight_status_convertion.keys():
            self.hl_flightStatus = flight_status_convertion[drone["flyingState"]]
          else: 
            self.hl_flightStatus = FLIGHT_STATUS.ONROUTE.value

          self.hl_pitch = 0.0
          self.hl_roll = 0.0
          self.hl_yaw = head_tmp * (180/pi)
          self.hl_satellite = 10.0
          self.hl_velocity = hl_velocity_class(drone.get("velocity"))
          pos = drone["positions"][1]
          self.hl_position = hl_position_class(lat=pos["latitude"], lon=pos["longitude"], asl=pos["altitude"], agl= pos["altitude"] - float(drone.get("takeoffAlt", 0)))
          self.hl_flightMode = FLIGHT_MODES.CONTROLLED.value
          self.hl_timestamp = datetime.utcnow().strftime(UTC_FORMAT)
          self.hl_fpid = fp_id



          # self.dsosId = awz_dsos_id_org_id
          # self.dronId = key_drone_name

          self.airwyaz_drone_id_in_data_drone = drone["id"]
          # self.flightTimeLeft = 1000#TODO ask shai if to implemment
          
          self.awz_pilot = data["pilot"]
          tmp_point = self.awz_pilot["position"]
          # self.homePoint = point3D(lat=tmp_point["latitude"], lon=tmp_point["longitude"], alt=tmp_point["altitude"])
          self.airwayz_drone_id_number = awz_obj["id"]
          
          
          # self.airwayz_model = drone.get("model","")
          # self.altATP = str(drone.get("takeoffAlt", 0))
          # self.airwyaz_description = drone.get("descriptors",[])
          # self.droneName = drone["name"]
          # self.flightMode = flightModeE.GCS 
          # self.inControl = True
          # self.flightStatus = flightStatus_dict.get(drone["flyingState"])

          # self.airwayz_is_controlable = drone.get("isControllable", True)
          # self.energy = float(drone["battery"]*100)
          # self.airwayz_attitude = drone["attitude"]
          # head_tmp = int(drone["attitude"]["yaw"])
          # if head_tmp < 0:
          #   head_tmp += 2*pi
          # self.heading = head_tmp * (180/pi)  # should be in anglels

          # if "velocity" in drone:
          #   self.velocity = float(drone["velocity"]["v_total"])
          # else:
          #   self.velocity = 1

          # self.takeOffPoint = point3D(lat=tmp_point["latitude"], lon=tmp_point["longitude"], alt=tmp_point["altitude"])

          # tmp_points_location = drone["positions"][1]
          # self.altASL = tmp_points_location.get("altitude")
          # self.location = point3D(lat=tmp_points_location["latitude"], lon=tmp_points_location["longitude"], alt=tmp_points_location["altitude"])

          # tmp_path = drone.get("path", {}).get("poi", [])
          # tmp_coords = []
          # for poi in tmp_path:
          #   tmp_coords.append([poi["longitude"], poi["latitude"], poi["altitude"]])

          # self.route = geoJsonLineString(coords=tmp_coords)

          # awz_alert = data.get("alerts", "")
          # awz_appversion = data.get("appVersion", "")
          # self.telemetryOriginTimestamp = data.get("time", "")

          # # awz_obj["chat"]

          # self.GPS_Status = GPS_StatusE.FIX
        except Exception as ex:
          print(ex, traceback.format_exc())


        return self







# class droneTelemetry:
#     """
#     "required": [
#     "dsosId",
#     "dronId",
#     "telemetryOriginTimestamp",
#     "droneType",
#     "droneName",
#     "energy",
#     "location",
#     "heading",
#     "velocity",
#     "altASL",
#     "homePoint",
#     "takeOffPoint",
#     "flightStatus",
#     "flightMode",
#     "GPS_Status",
#     "inControl"
#   ]
#     """
#     dsosId : str # "operator Id"
#     dronId : str # "the 4X string without the “4X”"
#     telemetryOriginTimestamp : str #"ISO format (ISO 8601) - the timestamp of telemetry creation (according to the server clock)"
#     droneType : droneTypeE # "the drone type and model.
#     droneName : str # "the name in the DSOS" "example": "flightOpps-1"
#     energy : float # "the % of energy left " 0-100
#     flightTimeLeft : int # "time flight left in seconds (regarding to energy left)
#     location : point3D # TBD /geo/geopointShort3D.v1.json
#     heading : float # 0.0 - 359.99 "The “nose” heading related to the True north"
#     velocity : float # minimum 0 horizontal velocity float or not float
#     altASL : float  #"Height above Sea lavel"
#     altATP : str #"Height above TakeOff point lavel"
#     homePoint : point3D #TBD /geo/geopointShort3D.v1.json
#     takeOffPoint : point3D #TBD /geo/geopointShort3D.v1.json
#     flightStatus : str # "Enum describing the flight status"
#     flightMode : flightModeE 
#     GPS_Status : GPS_StatusE
#     inControl : bool = True # "True if controlled by the DSOS, false if the drone is not controlled"
#     route : geoJsonLineString

#     # Airwayz objects
#     airwayz_drone_id_number : int
#     airwyaz_drone_id_in_data_drone : int
#     airwayz_model : str
#     airwayz_is_controlable : bool
#     airwyaz_description : list
#     airwayz_attitude : dict
    

#     # gets a dict data of drones and a key name, thoes extracted from the big object that as multipile objdects like this.
#     def init_from_awz(self, awz_dsos_id_org_id, key_drone_name, awz_obj):
#         # should dic {time: {}, enteties: {}} -> enteties
#         try:
#           self.dsosId = awz_dsos_id_org_id
#           self.dronId = key_drone_name

#           self.droneType = droneTypeE.dji
#           self.flightTimeLeft = 1000#TODO ask shai if to implemment
#           data = awz_obj["data"]
#           self.awz_pilot = data["pilot"]
#           tmp_point = self.awz_pilot["position"]
#           self.homePoint = point3D(lat=tmp_point["latitude"], lon=tmp_point["longitude"], alt=tmp_point["altitude"])
#           self.airwayz_drone_id_number = awz_obj["id"]
#           drone = data["drone"]
#           self.airwyaz_drone_id_in_data_drone = drone["id"]
#           self.airwayz_model = drone.get("model","")
#           self.altATP = str(drone.get("takeoffAlt", 0))
#           self.airwyaz_description = drone.get("descriptors",[])
#           self.droneName = drone["name"]
#           self.flightMode = flightModeE.GCS 
#           self.inControl = True
#           self.flightStatus = flightStatus_dict.get(drone["flyingState"])

#           self.airwayz_is_controlable = drone.get("isControllable", True)
#           self.energy = float(drone["battery"]*100)
#           self.airwayz_attitude = drone["attitude"]
#           head_tmp = int(drone["attitude"]["yaw"])
#           if head_tmp < 0:
#             head_tmp += 2*pi
#           self.heading = head_tmp * (180/pi)  # should be in anglels

#           if "velocity" in drone:
#             self.velocity = float(drone["velocity"]["v_total"])
#           else:
#             self.velocity = 1

#           self.takeOffPoint = point3D(lat=tmp_point["latitude"], lon=tmp_point["longitude"], alt=tmp_point["altitude"])

#           tmp_points_location = drone["positions"][1]
#           self.altASL = tmp_points_location.get("altitude")
#           self.location = point3D(lat=tmp_points_location["latitude"], lon=tmp_points_location["longitude"], alt=tmp_points_location["altitude"])

#           tmp_path = drone.get("path", {}).get("poi", [])
#           tmp_coords = []
#           for poi in tmp_path:
#             tmp_coords.append([poi["longitude"], poi["latitude"], poi["altitude"]])

#           self.route = geoJsonLineString(coords=tmp_coords)

#           awz_alert = data.get("alerts", "")
#           awz_appversion = data.get("appVersion", "")
#           self.telemetryOriginTimestamp = data.get("time", "")

#           # awz_obj["chat"]

#           self.GPS_Status = GPS_StatusE.FIX
#         except Exception as ex:
#           print(ex, traceback.format_exc())


#         return self


#     def init_from_sim(self, sim_dict):
#       try:
#         self.dsosId = sim_dict.get("dsosId") # some string
#         self.dronId = sim_dict.get("dronId") # "some string"
#         self.telemetryOriginTimestamp = sim_dict.get("telemetryOriginTimestamp") # iso time stamp
#         self.droneType = sim_dict.get("droneType") # string, dji or else
#         self.droneName = sim_dict.get("droneName") # string some name
#         self.energy = sim_dict.get("energy") # a number batery 
#         self.flightTimeLeft = sim_dict.get("flightTimeLeft")
#         self.heading = sim_dict.get("heading")

#         self.velocity = sim_dict.get("velocity")
#         self.altASL = sim_dict.get("altASL")
#         self.altATP = sim_dict.get("altATP")
#         self.GPS_Status = sim_dict.get("GPS_Status")

#         self.flightStatus = sim_dict.get("flightStatus")
#         self.flightMode = sim_dict.get("flightMode")

#         self.inControl = sim_dict.get("inControl")
#         # object
#         self.location = point3D(lat=sim_dict["location"]["lat"], lon=sim_dict["location"]["lon"], alt=sim_dict["location"]["alt"])
#         self.homePoint = point3D(lat=sim_dict["homePoint"]["lat"], lon=sim_dict["homePoint"]["lon"], alt=sim_dict["homePoint"]["alt"])
#         self.takeOffPoint = point3D(lat=sim_dict["takeOffPoint"]["lat"], lon=sim_dict["takeOffPoint"]["lon"], alt=sim_dict["takeOffPoint"]["alt"])

#         # path
#         rout_coords = sim_dict.get("route", {"geometry": {"coordinates": []}})["geometry"]["coordinates"]
#         self.route = geoJsonLineString(coords=rout_coords)

#         # Airwayz objects initialize
#         self.airwayz_drone_id_number =  132131
#         self.airwyaz_drone_id_in_data_drone = 2132132132
#         self.airwayz_model = self.droneType
#         self.airwayz_is_controlable = True
#         self.airwyaz_description = ["unmanned"]
#         if self.heading >=180:
#           self.heading -= 360
#         self.airwayz_attitude = {"yaw" : self.heading*(pi/180), "roll": 0, "pitch": 0, "time" : int(time.time())}
#       except Exception as ex:
#         print(ex, traceback.format_exc())

#       return self


#     def get_awz_dict(self):

#       """
#       should be in a format: 
#       {
#         "usses" : ...,
#         "nfz" : ...
#       }

#       """
#       # here the forma of usses dict
#       entety = {}
#       try:
#         drone = {}
#         pilot_position = self.homePoint.awz_get()
#         pilot_position["time"] = int(time.time())
#         pilot = {"id": "pilot_"+self.droneName,
#                 "callname": "cn_"+self.droneName,
#                 "position":  pilot_position}

#         drone["attitude"] = self.airwayz_attitude
#         drone["battery"] = float(int(self.energy) / 100)
#         drone["descriptors"] = ["unmanned"]
#         # drone["isManned"] = False
#         drone["flyingState"] = flightStatus_dict.get(self.flightStatus,"103")
#         drone["id"] = self.airwayz_drone_id_number
#         drone["isControllable"] = True

#         drone["model"] = self.droneType
#         drone["name"] = self.droneName

#         # path #TODO what to do with the nextPOI???
#         poi = self.route.awz_get(speed=self.velocity, default_height=self.altASL)
#         drone["path"] = {"nextPoiIndex": -1, "poi" : poi}

#         # positions #TODO need to calculate where the drone was
#         cur_location_dict = self.location.awz_get()
#         cur_location_dict["time"] = int(time.time()*1000)
#         cur_location_dict["alt_isa"] = 120.63
#         cur_location_dict["altitude_datum"] = "wgs84"
#         p1 = cur_location_dict # past
#         p1["time"] -= 1000
#         p2 = cur_location_dict # present
#         drone["positions"] = [p1, p2]

#         drone["velocity"] = {"vx": self.velocity, "vy": 0, "vz": 0, "v_total":self.velocity}

#         entety["chat"] = {}
#         entety["data"] = {"pilot" : pilot, "drone" : drone, "alerts" : {"collisions" : []}, "appVersion": "Airwayz-Simplex-Adapter"}
#         entety["id"] = self.airwayz_drone_id_number

#       except Exception as ex:
#         print(ex, traceback.format_exc())
#         return None

#       return entety

    
#     def get_sim_dic(self):
      
#       try:

#         dic = {
#         "dsosId": self.dsosId,
#         "dronId": self.dronId,
#         "telemetryOriginTimestamp": self.telemetryOriginTimestamp,
#         "droneType": "dji",
#         "droneName": self.droneName,
#         "energy": self.energy,
#         # "flightTimeLeft": 0, 
#         "location": self.location.get(),
#         "heading": self.heading,
#         "velocity": self.velocity,
#         "altASL": self.altASL,
#         "altATP": str(self.altATP),
#         "homePoint": self.homePoint.get(),
#         "takeOffPoint": self.takeOffPoint.get(),
#         "flightStatus": self.flightStatus, # dict - > str
#         "flightMode": "GCS", # enum
#         "GPS_Status": "FIX", # enum
#         "inControl": self.inControl
        
#         }
        

#       except Exception as ex:
#         print(ex, traceback.format_exc())
#         return None

#       tmp = self.route.get()
#       if len(tmp["geometry"]["coordinates"]) > 0:
#         dic["route"] = self.route.get()
      
#       return dic

if __name__ == "__main__":
  pass#print(GPS_StatusE.FIX.name)

import logging
from logging.handlers import RotatingFileHandler
from pathlib import Path

class adapter_logging:

    def __init__(self, file_name) -> None:
        self.filename = str(file_name)
        self.setup_logger()


    def setup_logger(self):
        currPath = str(Path(__file__).parent.absolute())
        logfilename = currPath + "/"+ self.filename + ".log"
        log = logging.getLogger(self.filename)

        handler = RotatingFileHandler(logfilename, maxBytes=50000000, backupCount=2)
        formatter = logging.Formatter('%(asctime)s : %(message)s \n')
        handler.setFormatter(formatter)
        log.setLevel(logging.DEBUG)
        log.addHandler(handler)

    def log_data(self, data, log_name=None, console=False, exc_info=None):
        if not console:
            log = logging.getLogger(self.filename)
            log.info(data, exc_info=exc_info)
        # else:
        #     print(data)


# server_logger = adapter_logging("server_logger")
# air_situation = adapter_logging("air_situation")
# telemtry = adapter_logging("telemtry")
# nfz = adapter_logging("air_situation")

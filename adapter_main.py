import signal,os
import time
import adapter_logger
from threading import Lock, Thread
from datetime import datetime, timedelta
from adapter_server import ThreadMain as USS_SERVER
from hl_nfz_reader import ThreadMain as AIR_SITUATION
from hl_telemtry_writer import ThreadMain as TELEMTRY
import awz_hl_api
from models.hl_global import AIR_SITUATION_TYPE

shared_data = {"HL_AP": [], "NFZ": [], "AIRWAYZ_AP": [], "token": "", "nfz_version": ""}
shared_data["USS_ID"] = "AZ" 
shared_data["TIME_DIFFRENCE"] = 0
shared_data["CONTAINER"] = []
shared_data["CORRIDOR"] = []
shared_data["AIRWAYZ_FPID"] = {}
shared_data["ALERT"] = {}


shared_data["VERSIONNUMBER"] = ""
shared_data["MAXRADIUSINMETERS"] = 400
shared_data["RADIUSINMETERS"] = 20
shared_data["FLIGHTPLANMAXHEIGHT"] = 120
shared_data["UAVALTITUDESEPARATION"] = 58



lock1 = Lock() # lock between ssh read/write and adapter
# Main code for the Adapter  module
# Activate three threads -  uss_server, simplex_reader, adapter.
# make them deomons so that it will automatically terminate with the main thread
# TO be implemented later:
# connect to UTM1, UTM2 or LAN UTM (debug), according the indices of the configuration files
# it is possible to connect to different units

class GracefulKiller: # allow proper exit with a ctrl-c or other termination signals


    def __init__(self):
        self.kill_now = False
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, *args):
        self.kill_now = True


if __name__ == "__main__":

    # Dummy is the shared data between the two child threads
    # locks are used to synchronize access in a thread safe manner.

    # make it a deamon so it will terminate when the main program exit
    tok = awz_hl_api.authenticate()
    print("token: ", tok)
    shared_data["token"] = tok
    policy = awz_hl_api.rules_get_rules(token=tok)
    hosttime = awz_hl_api.rules_get_time(token=tok)
    timenow = datetime.utcnow()
    
    
    time_diff = 0
    
    if hosttime is not None:
        time_diff = (timenow - hosttime).seconds
        print(f"Time diffrence in seconds: {time_diff}")
        print("Highlander Server time:" ,hosttime.strftime('%Y-%m-%dT%H:%M:%S.%fZ'))
        print("Airwayz Server time:", timenow.strftime('%Y-%m-%dT%H:%M:%S.%fZ'))

    shared_data["TIME_DIFFRENCE"] = time_diff
    
    print(f"Highlander POLICY:")
    if policy is not None:
    
        for k, v in policy.items():
            print(f"{str.upper(k)} : {v}")
            
    else:
        print("Error Policy request failed!!!")

    Thread(target = USS_SERVER,  args =(lock1, shared_data), daemon=True).start()
    Thread(target = TELEMTRY, args=(lock1, shared_data), daemon=True).start()
    # Thread(target = ALERT, args=(lock1, shared_data), daemon=True).start()
    Thread(target = AIR_SITUATION, args=(lock1, shared_data), daemon=True).start()

    killer = GracefulKiller()
    print("Adapter is Running")
    while not killer.kill_now:
        time.sleep(2)

    print("End of the program, we hope you enjoyed it")



import json
from gevent import config
from highlander_api.api_client import ApiClient, Configuration, Endpoint as _Endpoint
from highlander_api.api.general_api import GeneralApi
from highlander_api.model.inline_object import InlineObject as logging_object #usernam and pass
from highlander_api.model.inline_object1 import InlineObject1 as air_situation_type # air_situation_type
from highlander_api.model.inline_object10 import InlineObject10 # fp_id, departure_id, way_to_flight_plan (ApiEnUsFlightsDeactivateWayToFlightPlan):
from highlander_api.model.inline_object11 import InlineObject11 # reason (str): uav_serial_number (str): arrival_id (str): way_to_flight_plan (ApiEnUsFlightsDeactivateWayToFlightPlan):

from highlander_api.model.inline_object2 import InlineObject2 as alerts_type#  alerts_type (str):
from highlander_api.model.inline_object3 import InlineObject3 as ctr_id_type
from highlander_api.model.boundary import Boundary
from highlander_api.model.time_frame import TimeFrame
from highlander_api.model.position import Position
from highlander_api.model.inline_response20010_reason_boundary import InlineResponse20010ReasonBoundary as coordinats
from highlander_api.model.inline_object4 import InlineObject4 as ctr_object
from highlander_api.model.inline_object5 import InlineObject5 as update_ctr_object
from highlander_api.model.inline_object6 import InlineObject6 as delete_ctr_object
from highlander_api.model.inline_object7 import InlineObject7
from highlander_api.model.inline_object8 import InlineObject8
from highlander_api.model.inline_object9 import InlineObject9
from highlander_api.model.inline_response200 import InlineResponse200
from highlander_api.model.inline_response2001 import InlineResponse2001
from highlander_api.model.inline_response20010 import InlineResponse20010
from highlander_api.model.inline_response20011 import InlineResponse20011
from highlander_api.model.inline_response20012 import InlineResponse20012
from highlander_api.model.inline_response20013 import InlineResponse20013
from highlander_api.model.inline_response2002 import InlineResponse2002
from highlander_api.model.inline_response2003 import InlineResponse2003
from highlander_api.model.inline_response2004 import InlineResponse2004
from highlander_api.model.inline_response2005 import InlineResponse2005
from highlander_api.model.inline_response2006 import InlineResponse2006
from highlander_api.model.inline_response2007 import InlineResponse2007
from highlander_api.model.inline_response2008 import InlineResponse2008
from highlander_api.model.inline_response2009 import InlineResponse2009
from highlander_api.model.inline_response400 import InlineResponse400
from highlander_api.model.inline_response4001 import InlineResponse4001
from highlander_api.model.inline_response4002 import InlineResponse4002
from highlander_api.model.inline_response401 import InlineResponse401
from highlander_api.model.inline_response404 import InlineResponse404
from highlander_api.model.inline_response4041 import InlineResponse4041
from highlander_api.model.inline_response4042 import InlineResponse4042
from highlander_api.model.inline_response500 import InlineResponse500
from highlander_api.rest import RESTClientObject, RESTResponse


import requests
from datetime import datetime, timedelta
import traceback

HOST = "https://api.ussp.highlander.cloud"
USERNAME = "AZ"
PASS = "G0FAf9aa"

# ENDPOINT: /api/en-us/auth/authenticate
def authenticate(un=USERNAME, pas=PASS) -> str:
    log_obj = logging_object(un, pas)
    api = GeneralApi()
    k = {"inline_object": log_obj}
    res = api.authenticate(**k)
    token = ""
    try:
        if res.get("type") == 200 and "reason" in res and "token" in res["reason"]:
            log_data("logging succeed")
            token = res["reason"]["token"]
        else:
            log_data("logging failed")
            log_data(res)

    except Exception as ex:
        log_data("logging failed")
        log_data(f"Error in authenticate@{__name__}: {ex}")
    
    return token


# ENDPOINT: /api/en-us/air-situation/get
# TODO need to convert data reeturn from that
def get_air_situation(token="", opt='none'):
    try:
        cofnig = Configuration(access_token=token)
        cli = ApiClient(configuration=cofnig)
        api = GeneralApi(cli)

        ast = air_situation_type(air_situation_type=opt)

        url = HOST + api.air_situation_get_endpoint.settings["endpoint_path"]
        head = {"Authorization": "Bearer "+token}
        r = requests.post(url, json={"airSituationType":opt}, headers=head)
        if r.status_code != 500:
            res = json.loads(r.text)
        else:
            res = r.text

        

    except Exception as ex:
        log_data(f"Error get_air_situation@{__name__}: {ex}")
    finally:
        return res

    

# ENDPOINT: /api/en-us/alerts/get
# return: {'collision': [], 'suggestion': [], 'system': []}
def get_alert(token="", opt='FULL'):
    try:
        cofnig = Configuration(access_token=token)
        cli = ApiClient(configuration=cofnig)
        api = GeneralApi(cli)

        options = {
                'FULL': "full",
                'UAV': "uav",
            }

        ast = alerts_type(alerts_type=options[opt])
        k = {"inline_object2": ast}

        url = HOST + api.alerts_get_endpoint.settings["endpoint_path"]
        head = {"Authorization": "Bearer "+token}
        r = requests.post(url, json=ast.to_dict(), headers=head)
        res = json.loads(r.text)

        return res
    except Exception as ex:
        log_data(f"Error get_alert@{__name__}: {ex}")
    

# ENDPOINT: /api/en-us/rules/getTime
def rules_get_time(token="") -> datetime:
    cofnig = Configuration(access_token=token)
    
    cli = ApiClient(configuration=cofnig)
    api = GeneralApi(cli)
    k = {}
    
    # _url = HOST + api.rules_get_time_endpoint.settings["endpoint_path"]
    # print(_url)
    # head = {"Authorization": "Bearer "+token}
    # r = requests.get(url=_url, headers=head)
    # print(r.text)
    
    res = api.rules_get_time(**k)

    try:
        if res.get("type") == 200 and "reason" in res:
            time_str = res["reason"]
            return datetime.strptime(time_str, '%Y-%m-%dT%H:%M:%S.%fZ')
        else:
            log_data(res)

    except Exception as ex:
        log_data(f"Error rules_get_time@{__name__}: {ex}")
    
    return None

# ENDPOINT: /api/en-us/rules/get
def rules_get_rules(token=""):
    cofnig = Configuration(access_token=token)
    cli = ApiClient(configuration=cofnig)
    api = GeneralApi(cli)
    k = {}
    _url = HOST + api.rules_get_endpoint.settings["endpoint_path"]
    
    head = {"Authorization": "Bearer "+token}
    r = requests.get(url=_url, headers=head)
    res = json.loads(r.text)

    # res = api.rules_get(**k)

    try:
        if res.get("type") == 200 and "reason" in res:
            rules = res["reason"]
            return rules
        else:
            log_data(res)

    except Exception as ex:
        log_data(f"Error rules_get_rules@{__name__}: {ex}")
    
    return None

# ENDPOINT: /api/en-us/geofences/get
def get_ctr_geofences(token="", ctr_id=""):
    cofnig = Configuration(access_token=token)
    cli = ApiClient(configuration=cofnig)
    api = GeneralApi(cli)
    ctr_id_type_obj = ctr_id_type(id=ctr_id)
    k = {"inline_object3": ctr_id_type_obj}
    res = api.ctr_get(**k)

    try:
        if res.get("type") == 200 and "reason" in res:
            ctr = res["reason"]
            return ctr
        else:
            log_data(res)

    except Exception as ex:
        log_data(f"Error get_ctr_geofences@{__name__}: {ex}")
    
    return None

# ENDPOINT: /api/en-us/geofences/add
def add_ctr_geofences(token="", ctr_name="Airwayz_test1_ctr", bound=[], starttime="", endtime=""):
    cofnig = Configuration(access_token=token)
    cli = ApiClient(configuration=cofnig)
    api = GeneralApi(cli)
    

    ______temp_dict = [{"lat": 31.240104757766613, "lon": 34.85687255859375},
                       {"lat": 31.240104757766613, "lon": 34.85942602157593},
                       {"lat": 31.242159525589464, "lon": 34.85942602157593},
                       {"lat": 31.242159525589464, "lon": 34.85687255859375}]
    
    list_of_poi = []
    for item in ______temp_dict:
        coord = coordinats(latitude=item["lat"], longitude=item["lon"])
        tmp_poi = Position(100.0, 100.0, coordinates=coord)
        # print(tmp_poi.to_dict())
        list_of_poi.append(tmp_poi.to_dict())

    boundry_args = {"value" : list_of_poi}
    if starttime == "" or endtime == "":
        _starttime = datetime.utcnow()
        _endtime = (datetime.utcnow() + timedelta(days=3))
        starttime = datetime.strftime(_starttime, '%Y-%m-%dT%H:%M:%S.%fZ')
        endtime = datetime.strftime(_endtime, '%Y-%m-%dT%H:%M:%S.%fZ')

    b = Boundary(**boundry_args)
    
    tf = TimeFrame(start_date=starttime, end_date=endtime)
    tf.to_dict()
    kk = {"name": ctr_name, "boundary": b, "time_frame": tf,"type": "ctr"}
    ctr_id_type_obj = ctr_object(**kk)
    k = {"inline_object4": ctr_id_type_obj}

    body = {}
    body["name"] = ctr_name
    body["type"] = "ctr"
    polygon = list_of_poi
    body["boundary"] = [{"polygon": polygon}]
    body["timeFrame"] = {"startDate": starttime, "endDate":endtime}
    url = HOST + api.ctr_add_endpoint.settings["endpoint_path"]
    head = {"Authorization": "Bearer "+token}
    r = requests.post(url, json=body, headers=head)
    res = json.loads(r.text)

    # res = api.ctr_add(**k)

    try:
        if res.get("type") == 200 and "reason" in res:
            ctr = res["reason"]
            return ctr
        else:
            log_data(res)

    except Exception as ex:
        log_data(f"Error add_ctr_geofences@{__name__}: {ex}")
    
    return None
# ENDPOINT: /api/en-us/geofences/update
def update_ctr_geofences(token="",id="", ctr_name="Airwayz_test1", bound=[], starttime="", endtime=""):
    cofnig = Configuration(access_token=token)
    cli = ApiClient(configuration=cofnig)
    api = GeneralApi(cli)
    

    _temp_dict = [{"lat": 31.240104757766613, "lon": 34.85687255859375},
                       {"lat": 31.240104757766613, "lon": 34.86942602157593},
                       {"lat": 31.242159525589464, "lon": 34.85942602157593},
                       {"lat": 31.242159525589464, "lon": 34.85687255859375}]
    
    list_of_poi = []
    for item in _temp_dict:
        coord = coordinats(latitude=item["lat"], longitude=item["lon"])
        tmp_poi = Position(100.0, 100.0, coordinates=coord)
        list_of_poi.append(tmp_poi.to_dict())

    if starttime == "" or endtime == "":
        _starttime = datetime.utcnow()
        _endtime = (datetime.utcnow() + timedelta(days=3))
        starttime = datetime.strftime(_starttime, '%Y-%m-%dT%H:%M:%S.%fZ')
        endtime = datetime.strftime(_endtime, '%Y-%m-%dT%H:%M:%S.%fZ')


    url = HOST + api.ctr_update_endpoint.settings["endpoint_path"]
    head = {"Authorization": "Bearer "+token}


    body = {}
    body["name"] = ctr_name
    body["type"] = "ctr"
    polygon = list_of_poi
    body["id"] = id
    body["boundary"] = [{"polygon": polygon}]
    body["timeFrame"] = {"startDate": starttime, "endDate":endtime}

    r = requests.post(url, json=body, headers=head)
    res = json.loads(r.text)

    # res = api.ctr_update(**k)
    print(res)
    try:
        if res.get("type") == 200 and "reason" in res:
            ctr = res["reason"]
            return ctr
        else:
            log_data(res)

    except Exception as ex:
        log_data(f"Error update_ctr_geofences@{__name__}: {ex}")
    
    return None
# ENDPOINT: /api/en-us/geofences/delete
def delete_ctr_geofences(token="", id="22"):
    cofnig = Configuration(access_token=token)
    cli = ApiClient(configuration=cofnig)
    api = GeneralApi(cli)
    
    kk = {"id": id}
    ctr_id_type_obj = delete_ctr_object(**kk)
    k = {"inline_object6": ctr_id_type_obj}

    url = HOST + api.ctr_delete_endpoint.settings["endpoint_path"]
    head = {"Authorization": "Bearer "+token}
    body = ctr_id_type_obj.to_dict()
    body["id"] = id

    res = api.ctr_delete(**k)
    print(res)

    try:
        if res.get("type") == 200 and "reason" in res:
            ctr = res["reason"]
            return ctr
        else:
            log_data(res)

    except Exception as ex:
        log_data(f"Error  delete_ctr_geofences@{__name__}: {ex}")
    
    return None

# ENDPOINT: /api/en-us/uavs/telemetry
#TODO need to implement the return values
def uavs_telemtry(token="", drones=None, airSituationType='none'):
    try:
        cofnig = Configuration(access_token=token)
        cli = ApiClient(configuration=cofnig)
        api = GeneralApi(cli)

        url = HOST + api.telemetry_endpoint.settings["endpoint_path"]
        head = {"Authorization": "Bearer "+token}
        body = {}
        body["airSituationType"] = airSituationType
        body["telemetry"] = drones
        r = requests.post(url, json=body, headers=head)

        if r.status_code != 500:
            res = json.loads(r.text)
        else:
            res = r.text

    except Exception as ex:
        log_data(f"Error uavs_telemtry@{__name__}: \n{ex}\n response value {r}")
    
    return res
    

# ENDPOINT: /api/en-us/flights/get
def get_flights(token="", fp_id="fpid"):
    cofnig = Configuration(access_token=token)
    cli = ApiClient(configuration=cofnig)
    api = GeneralApi(cli)
    

    url = HOST + api.flights_get_endpoint.settings["endpoint_path"]
    head = {"Authorization": "Bearer "+token}
    body = {"id": fp_id}
    r = requests.post(url, json=body, headers=head)
    res = json.loads(r.text)

    return res

# ENDPOINT: /api/en-us/flights/register
def register_flights(token="", reqFlightPlan=None):
    try:
        cofnig = Configuration(access_token=token)
        cli = ApiClient(configuration=cofnig)
        api = GeneralApi(cli)


        url = HOST + api.flights_register_endpoint.settings["endpoint_path"]
        head = {"Authorization": "Bearer "+token}
        body = reqFlightPlan.to_dict()
        r = requests.post(url, json=body, headers=head)
        res = json.loads(r.text)

        return res

            
    except Exception as ex:
        log_data(f"Error register_flights@{__name__}: {ex}")
    
    return None
# ENDPOINT: /api/en-us/flights/activate
#TODO need to implement bridge
def activate_flights(token="", fp_id="fpid", dep_id="WxbpUbIYRPlSTsNY3P9R", is_inside_ctr=True, bridge=None):
    try:
        cofnig = Configuration(access_token=token)
        cli = ApiClient(configuration=cofnig)
        api = GeneralApi(cli)


        url = HOST + api.flights_activate_endpoint.settings["endpoint_path"]
        head = {"Authorization": "Bearer "+token}
        body = {}
        body["fpId"] = fp_id
        body["departureId"] = dep_id
        body["wayToFlightPlan"] = bridge
        r = requests.post(url, json=body, headers=head)
        res = json.loads(r.text)

        return res


    except Exception as ex:
        log_data(f"Error activate_flights@{__name__}: {ex}")

# ENDPOINT: /api/en-us/flights/deactivate
def deactivate_flights(token="", fp_id="fpid", drone_id="droneid"):
    try:
        cofnig = Configuration(access_token=token)
        cli = ApiClient(configuration=cofnig)
        api = GeneralApi(cli)


        url = HOST + api.flights_deactivate_endpoint.settings["endpoint_path"]
        head = {"Authorization": "Bearer "+token}
        body = {}
        body["id"] = fp_id
        body["reason"] = 'shut_down' #TODO ask shai what to give here? [finished, finished, finished, finished] https://api.ussp.highlander.cloud/global.html#FLIGHT_PLAN_TERMINATION_REASON
        body["uavSerialNumber"] = drone_id
        # body["arrivalId"] = '' # Optional. Only if reason is RTH
        # body["wayToFlightPlan"] = '' # Optional. Only if reason is RTH
        r = requests.post(url, json=body, headers=head)
        res = json.loads(r.text)

        # res = api.flights_deactivate(**k)
        return res

    except Exception as ex:
        log_data(f"Error deactivate_flights@{__name__}: {ex}")
    

def log_data(m=""):
    print(m, flush=True)

if __name__ == "__main__":
    pass
    tok = authenticate()
    # print(tok)
    # tok = "U2FsdGVkX1+ujj2/PJPbMGiIwmtpPlTjRMOoEsXsS+SpJV3sGAVLR7WDa2kW8hZbe7y4xzSUnmbwKd2D55JHVjTJlJekTdhE/31Lq/dm9sEuvS8WjhWPC9IjJ7wZrbAL"
    # print(add_ctr_geofences(token=tok))
    some_id = 'K2zv9B2QIU3MO8h9T5Yw'
    # print(update_ctr_geofences(token=tok, id=some_id))
    # print(delete_ctr_geofences(token=tok, id=some_id))
    # print(add_ctr_geofences(token=tok))
    # print(tok)
    # tok = "U2FsdGVkX1+6RH/OnxjLDmdFB+Wrlvm4mr1qvrZOCJB3iodo6cUF12sPuUEOgUkGwdI0TAmQ06osvLDZ+LbDpXXm7REFuWPocjspZdqMmzz9+OcLlSN8fxOoEDQhMGXP"
    res = get_air_situation(tok, opt='full')

    # print(res)
    # import json
    # _res = get_air_situation(tok, opt='full')
    # res = _res.get("reason", {})
    # bridge = res.get("bridge", None)
    # container = res.get("container", None)
    nfz = res.get("nfz", None)
    ctr = res.get("ctr", None)
    # uavs = res.get("uavs", None)
    # dynamic_nfz = res.get("dynamic_nfz", None)
    # flight_plan = res.get("flight_plan", None)
    # staticAirSpaceVersionNumber = res.get("staticAirSpaceVersionNumber", None)
    # # print("bridge:",json.dumps(bridge))
    
    # # print("container:",json.dumps(container))
    print("nfz:", json.dumps(nfz))
    print("ctr:", json.dumps(ctr))
    # print("uavs:", json.dumps(uavs))
    # print("dynamic_nfz:", json.dumps(dynamic_nfz))
    # print("flight_plan:", json.dumps(flight_plan))
    # print("staticAirSpaceVersionNumber:", json.dumps(staticAirSpaceVersionNumber))
    # type_option = {'FULL': "full",
    #         'STATIC': "static",
    #         'DYNAMIC': "dynamic",
    # 'NONE': "none"}

    # res = add_ctr_geofences(tok, ctr_name="Airwayz_test2")
    # print(res)
# highlander-api
In the following doc we will cover the High Lander USSP method that enables to integrate drones into our life in day-to-day missions.

Drones are a growing business in the world, delivering services in all environments, including urban areas.
A clear framework to integrate drones safely and securely would allow the creation of a truly worldwide market for drone services and aircraft.

This Python package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.0.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.PythonClientCodegen

## Requirements.

Python >=3.6

## Installation & Usage
### pip install

If the python package is hosted on a repository, you can install directly using:

```sh
pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

Then import the package:
```python
import highlander_api
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import highlander_api
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python

import time
import highlander_api
from pprint import pprint
from highlander_api.api import general_api
from highlander_api.model.inline_object import InlineObject
from highlander_api.model.inline_object1 import InlineObject1
from highlander_api.model.inline_object10 import InlineObject10
from highlander_api.model.inline_object11 import InlineObject11
from highlander_api.model.inline_object2 import InlineObject2
from highlander_api.model.inline_object3 import InlineObject3
from highlander_api.model.inline_object4 import InlineObject4
from highlander_api.model.inline_object5 import InlineObject5
from highlander_api.model.inline_object6 import InlineObject6
from highlander_api.model.inline_object7 import InlineObject7
from highlander_api.model.inline_object8 import InlineObject8
from highlander_api.model.inline_object9 import InlineObject9
from highlander_api.model.inline_response200 import InlineResponse200
from highlander_api.model.inline_response2001 import InlineResponse2001
from highlander_api.model.inline_response20010 import InlineResponse20010
from highlander_api.model.inline_response20011 import InlineResponse20011
from highlander_api.model.inline_response20012 import InlineResponse20012
from highlander_api.model.inline_response20013 import InlineResponse20013
from highlander_api.model.inline_response2002 import InlineResponse2002
from highlander_api.model.inline_response2003 import InlineResponse2003
from highlander_api.model.inline_response2004 import InlineResponse2004
from highlander_api.model.inline_response2005 import InlineResponse2005
from highlander_api.model.inline_response2006 import InlineResponse2006
from highlander_api.model.inline_response2007 import InlineResponse2007
from highlander_api.model.inline_response2008 import InlineResponse2008
from highlander_api.model.inline_response2009 import InlineResponse2009
from highlander_api.model.inline_response400 import InlineResponse400
from highlander_api.model.inline_response4001 import InlineResponse4001
from highlander_api.model.inline_response4002 import InlineResponse4002
from highlander_api.model.inline_response401 import InlineResponse401
from highlander_api.model.inline_response404 import InlineResponse404
from highlander_api.model.inline_response4041 import InlineResponse4041
from highlander_api.model.inline_response4042 import InlineResponse4042
from highlander_api.model.inline_response500 import InlineResponse500
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)


# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
content_type = "application/json" # str | application/json (optional)
inline_object1 = InlineObject1(
        air_situation_type="full",
    ) # InlineObject1 |  (optional)

    try:
        # get air situation
        api_response = api_instance.air_situation_get(accept=accept, content_type=content_type, inline_object1=inline_object1)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->air_situation_get: %s\n" % e)
```

## Documentation for API Endpoints

All URIs are relative to *https://api.ussp.highlander.cloud*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*GeneralApi* | [**air_situation_get**](docs/GeneralApi.md#air_situation_get) | **POST** /api/en-us/air-situation/get | get air situation
*GeneralApi* | [**alerts_get**](docs/GeneralApi.md#alerts_get) | **POST** /api/en-us/alerts/get | get alerts
*GeneralApi* | [**authenticate**](docs/GeneralApi.md#authenticate) | **POST** /api/en-us/auth/authenticate | authenticate
*GeneralApi* | [**ctr_add**](docs/GeneralApi.md#ctr_add) | **POST** /api/en-us/geofences/add | add CTR
*GeneralApi* | [**ctr_delete**](docs/GeneralApi.md#ctr_delete) | **POST** /api/en-us/geofences/delete | delete CTR
*GeneralApi* | [**ctr_get**](docs/GeneralApi.md#ctr_get) | **POST** /api/en-us/geofences/get | get CTR
*GeneralApi* | [**ctr_update**](docs/GeneralApi.md#ctr_update) | **POST** /api/en-us/geofences/update | update CTR
*GeneralApi* | [**flights_activate**](docs/GeneralApi.md#flights_activate) | **POST** /api/en-us/flights/activate | activate flight plan
*GeneralApi* | [**flights_deactivate**](docs/GeneralApi.md#flights_deactivate) | **POST** /api/en-us/flights/deactivate | deactivate flight plan
*GeneralApi* | [**flights_get**](docs/GeneralApi.md#flights_get) | **POST** /api/en-us/flights/get | get flight plans
*GeneralApi* | [**flights_register**](docs/GeneralApi.md#flights_register) | **POST** /api/en-us/flights/register | register flight plan
*GeneralApi* | [**rules_get**](docs/GeneralApi.md#rules_get) | **GET** /api/en-us/rules/get | 
*GeneralApi* | [**rules_get_time**](docs/GeneralApi.md#rules_get_time) | **GET** /api/en-us/rules/getTime | get Time
*GeneralApi* | [**telemetry**](docs/GeneralApi.md#telemetry) | **POST** /api/en-us/uavs/telemetry | set telemetry


## Documentation For Models

 - [ApiEnUsFlightsActivateWayToFlightPlan](docs/ApiEnUsFlightsActivateWayToFlightPlan.md)
 - [ApiEnUsFlightsDeactivateWayToFlightPlan](docs/ApiEnUsFlightsDeactivateWayToFlightPlan.md)
 - [ApiEnUsUavsTelemetryPilot](docs/ApiEnUsUavsTelemetryPilot.md)
 - [ApiEnUsUavsTelemetryTelemetry](docs/ApiEnUsUavsTelemetryTelemetry.md)
 - [ApiEnUsUavsTelemetryVelocity](docs/ApiEnUsUavsTelemetryVelocity.md)
 - [Boundary](docs/Boundary.md)
 - [InlineObject](docs/InlineObject.md)
 - [InlineObject1](docs/InlineObject1.md)
 - [InlineObject10](docs/InlineObject10.md)
 - [InlineObject11](docs/InlineObject11.md)
 - [InlineObject2](docs/InlineObject2.md)
 - [InlineObject3](docs/InlineObject3.md)
 - [InlineObject4](docs/InlineObject4.md)
 - [InlineObject5](docs/InlineObject5.md)
 - [InlineObject6](docs/InlineObject6.md)
 - [InlineObject7](docs/InlineObject7.md)
 - [InlineObject8](docs/InlineObject8.md)
 - [InlineObject9](docs/InlineObject9.md)
 - [InlineResponse200](docs/InlineResponse200.md)
 - [InlineResponse2001](docs/InlineResponse2001.md)
 - [InlineResponse20010](docs/InlineResponse20010.md)
 - [InlineResponse20010Reason](docs/InlineResponse20010Reason.md)
 - [InlineResponse20010ReasonBoundary](docs/InlineResponse20010ReasonBoundary.md)
 - [InlineResponse20010ReasonStatus](docs/InlineResponse20010ReasonStatus.md)
 - [InlineResponse20010ReasonZones](docs/InlineResponse20010ReasonZones.md)
 - [InlineResponse20011](docs/InlineResponse20011.md)
 - [InlineResponse20011Reason](docs/InlineResponse20011Reason.md)
 - [InlineResponse20012](docs/InlineResponse20012.md)
 - [InlineResponse20012Reason](docs/InlineResponse20012Reason.md)
 - [InlineResponse20012ReasonStatus](docs/InlineResponse20012ReasonStatus.md)
 - [InlineResponse20013](docs/InlineResponse20013.md)
 - [InlineResponse20013Reason](docs/InlineResponse20013Reason.md)
 - [InlineResponse20013ReasonStatus](docs/InlineResponse20013ReasonStatus.md)
 - [InlineResponse2001Reason](docs/InlineResponse2001Reason.md)
 - [InlineResponse2001ReasonBridge](docs/InlineResponse2001ReasonBridge.md)
 - [InlineResponse2001ReasonBridgesIds](docs/InlineResponse2001ReasonBridgesIds.md)
 - [InlineResponse2001ReasonContainer](docs/InlineResponse2001ReasonContainer.md)
 - [InlineResponse2001ReasonCtr](docs/InlineResponse2001ReasonCtr.md)
 - [InlineResponse2001ReasonNfz](docs/InlineResponse2001ReasonNfz.md)
 - [InlineResponse2002](docs/InlineResponse2002.md)
 - [InlineResponse2002Reason](docs/InlineResponse2002Reason.md)
 - [InlineResponse2003](docs/InlineResponse2003.md)
 - [InlineResponse2004](docs/InlineResponse2004.md)
 - [InlineResponse2004Reason](docs/InlineResponse2004Reason.md)
 - [InlineResponse2005](docs/InlineResponse2005.md)
 - [InlineResponse2005Reason](docs/InlineResponse2005Reason.md)
 - [InlineResponse2006](docs/InlineResponse2006.md)
 - [InlineResponse2007](docs/InlineResponse2007.md)
 - [InlineResponse2008](docs/InlineResponse2008.md)
 - [InlineResponse2009](docs/InlineResponse2009.md)
 - [InlineResponse2009Reason](docs/InlineResponse2009Reason.md)
 - [InlineResponse2009ReasonAirSituation](docs/InlineResponse2009ReasonAirSituation.md)
 - [InlineResponse2009ReasonAirSituationBridge](docs/InlineResponse2009ReasonAirSituationBridge.md)
 - [InlineResponse2009ReasonAirSituationBridgesIds](docs/InlineResponse2009ReasonAirSituationBridgesIds.md)
 - [InlineResponse2009ReasonAirSituationContainer](docs/InlineResponse2009ReasonAirSituationContainer.md)
 - [InlineResponse2009ReasonAirSituationCtr](docs/InlineResponse2009ReasonAirSituationCtr.md)
 - [InlineResponse2009ReasonAirSituationNfz](docs/InlineResponse2009ReasonAirSituationNfz.md)
 - [InlineResponse2009ReasonAlerts](docs/InlineResponse2009ReasonAlerts.md)
 - [InlineResponse200Reason](docs/InlineResponse200Reason.md)
 - [InlineResponse400](docs/InlineResponse400.md)
 - [InlineResponse4001](docs/InlineResponse4001.md)
 - [InlineResponse4002](docs/InlineResponse4002.md)
 - [InlineResponse4002Bridge](docs/InlineResponse4002Bridge.md)
 - [InlineResponse4002BridgesIds](docs/InlineResponse4002BridgesIds.md)
 - [InlineResponse4002Errors](docs/InlineResponse4002Errors.md)
 - [InlineResponse4002Suggestions](docs/InlineResponse4002Suggestions.md)
 - [InlineResponse4002SuggestionsWayToFlightPlan](docs/InlineResponse4002SuggestionsWayToFlightPlan.md)
 - [InlineResponse401](docs/InlineResponse401.md)
 - [InlineResponse404](docs/InlineResponse404.md)
 - [InlineResponse4041](docs/InlineResponse4041.md)
 - [InlineResponse4042](docs/InlineResponse4042.md)
 - [InlineResponse500](docs/InlineResponse500.md)
 - [Position](docs/Position.md)
 - [TimeFrame](docs/TimeFrame.md)


## Documentation For Authorization


## bearerAuth

- **Type**: Bearer authentication


## Author

adam@highlander.io


## Notes for Large OpenAPI documents
If the OpenAPI document is large, imports in highlander_api.apis and highlander_api.models may fail with a
RecursionError indicating the maximum recursion limit has been exceeded. In that case, there are a couple of solutions:

Solution 1:
Use specific imports for apis and models like:
- `from highlander_api.api.default_api import DefaultApi`
- `from highlander_api.model.pet import Pet`

Solution 2:
Before importing the package, adjust the maximum recursion limit as shown below:
```
import sys
sys.setrecursionlimit(1500)
import highlander_api
from highlander_api.apis import *
from highlander_api.models import *
```


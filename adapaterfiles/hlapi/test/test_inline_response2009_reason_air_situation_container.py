"""
    api

    In the following doc we will cover the High Lander USSP method that enables to integrate drones into our life in day-to-day missions.  Drones are a growing business in the world, delivering services in all environments, including urban areas. A clear framework to integrate drones safely and securely would allow the creation of a truly worldwide market for drone services and aircraft.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Contact: adam@highlander.io
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import highlander_api
from highlander_api.model.boundary import Boundary
from highlander_api.model.position import Position
from highlander_api.model.time_frame import TimeFrame
globals()['Boundary'] = Boundary
globals()['Position'] = Position
globals()['TimeFrame'] = TimeFrame
from highlander_api.model.inline_response2009_reason_air_situation_container import InlineResponse2009ReasonAirSituationContainer


class TestInlineResponse2009ReasonAirSituationContainer(unittest.TestCase):
    """InlineResponse2009ReasonAirSituationContainer unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testInlineResponse2009ReasonAirSituationContainer(self):
        """Test InlineResponse2009ReasonAirSituationContainer"""
        # FIXME: construct object with mandatory attributes with example values
        # model = InlineResponse2009ReasonAirSituationContainer()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

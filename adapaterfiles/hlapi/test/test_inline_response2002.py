"""
    api

    In the following doc we will cover the High Lander USSP method that enables to integrate drones into our life in day-to-day missions.  Drones are a growing business in the world, delivering services in all environments, including urban areas. A clear framework to integrate drones safely and securely would allow the creation of a truly worldwide market for drone services and aircraft.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Contact: adam@highlander.io
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import highlander_api
from highlander_api.model.inline_response2002_reason import InlineResponse2002Reason
globals()['InlineResponse2002Reason'] = InlineResponse2002Reason
from highlander_api.model.inline_response2002 import InlineResponse2002


class TestInlineResponse2002(unittest.TestCase):
    """InlineResponse2002 unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testInlineResponse2002(self):
        """Test InlineResponse2002"""
        # FIXME: construct object with mandatory attributes with example values
        # model = InlineResponse2002()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

"""
    api

    In the following doc we will cover the High Lander USSP method that enables to integrate drones into our life in day-to-day missions.  Drones are a growing business in the world, delivering services in all environments, including urban areas. A clear framework to integrate drones safely and securely would allow the creation of a truly worldwide market for drone services and aircraft.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Contact: adam@highlander.io
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import highlander_api
from highlander_api.model.inline_response20011_reason import InlineResponse20011Reason
globals()['InlineResponse20011Reason'] = InlineResponse20011Reason
from highlander_api.model.inline_response20011 import InlineResponse20011


class TestInlineResponse20011(unittest.TestCase):
    """InlineResponse20011 unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testInlineResponse20011(self):
        """Test InlineResponse20011"""
        # FIXME: construct object with mandatory attributes with example values
        # model = InlineResponse20011()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

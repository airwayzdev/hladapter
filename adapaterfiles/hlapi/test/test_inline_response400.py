"""
    api

    In the following doc we will cover the High Lander USSP method that enables to integrate drones into our life in day-to-day missions.  Drones are a growing business in the world, delivering services in all environments, including urban areas. A clear framework to integrate drones safely and securely would allow the creation of a truly worldwide market for drone services and aircraft.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Contact: adam@highlander.io
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import highlander_api
from highlander_api.model.inline_response400 import InlineResponse400


class TestInlineResponse400(unittest.TestCase):
    """InlineResponse400 unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testInlineResponse400(self):
        """Test InlineResponse400"""
        # FIXME: construct object with mandatory attributes with example values
        # model = InlineResponse400()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

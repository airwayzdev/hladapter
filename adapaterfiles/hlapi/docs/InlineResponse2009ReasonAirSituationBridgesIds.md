# InlineResponse2009ReasonAirSituationBridgesIds


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**evr9_anxui8_id_vj_myx_tqb** | **str** |  | 
**x_vsl4_xo_ejbp_ov3_jeh_dd_p** | **str** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



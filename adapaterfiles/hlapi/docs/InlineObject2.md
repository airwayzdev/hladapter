# InlineObject2


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alerts_type** | **str** |  | 
**tail_number** | **str** | Should only be sent when alertsType is &#39;uav&#39; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



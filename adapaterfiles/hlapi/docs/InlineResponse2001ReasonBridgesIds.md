# InlineResponse2001ReasonBridgesIds


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**x3f_ok_f0l_ku27d2eanbtb** | **str** |  | 
**mmrro_k6_ji0_revl6_xql15** | **str** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



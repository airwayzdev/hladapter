# ApiEnUsUavsTelemetryTelemetry


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**model** | **str** |  | 
**tail_number** | **str** | Tail number of the UAV. &lt;br/&gt;This is a unique value as registered and recieved by the Airspace authorities | 
**serial_number** | **str** |  | 
**type** | **str** |  | 
**battery** | **float** | Battery of the UAV. Should be between 0 to 100 percent. | 
**heading** | **float** | Heading of the UAV. Should be between -180 to 180 degrees. | 
**is_flying** | **bool** |  | 
**satellite** | **float** |  | 
**pitch** | **float** |  | 
**roll** | **float** |  | 
**yaw** | **float** |  | 
**flight_mode** | **str** |  | 
**timestamp** | **str** |  | 
**sign** | **str** |  | 
**takeoff_location** | [**Position**](Position.md) |  | [optional] 
**flight_status** | **str** |  | [optional] 
**velocity** | [**ApiEnUsUavsTelemetryVelocity**](ApiEnUsUavsTelemetryVelocity.md) |  | [optional] 
**position** | [**Position**](Position.md) |  | [optional] 
**up_link_remote_controller_signal_quality** | **float** |  | [optional] 
**waypoint_index** | **float** |  | [optional] 
**pilot** | [**ApiEnUsUavsTelemetryPilot**](ApiEnUsUavsTelemetryPilot.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# InlineResponse2009ReasonAirSituationCtr


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**created_at** | **str** |  | 
**name** | **str** |  | 
**type** | **str** |  | defaults to "ctr"
**center_point** | [**Position**](Position.md) |  | [optional] 
**boundary** | [**Boundary**](Boundary.md) |  | [optional] 
**time_frame** | [**TimeFrame**](TimeFrame.md) |  | [optional] 
**tags** | **[str]** |  | [optional] 
**dsos_ids** | **[str]** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



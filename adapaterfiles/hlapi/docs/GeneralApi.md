# highlander_api.GeneralApi

All URIs are relative to *https://api.ussp.highlander.cloud*

Method | HTTP request | Description
------------- | ------------- | -------------
[**air_situation_get**](GeneralApi.md#air_situation_get) | **POST** /api/en-us/air-situation/get | get air situation
[**alerts_get**](GeneralApi.md#alerts_get) | **POST** /api/en-us/alerts/get | get alerts
[**authenticate**](GeneralApi.md#authenticate) | **POST** /api/en-us/auth/authenticate | authenticate
[**ctr_add**](GeneralApi.md#ctr_add) | **POST** /api/en-us/geofences/add | add CTR
[**ctr_delete**](GeneralApi.md#ctr_delete) | **POST** /api/en-us/geofences/delete | delete CTR
[**ctr_get**](GeneralApi.md#ctr_get) | **POST** /api/en-us/geofences/get | get CTR
[**ctr_update**](GeneralApi.md#ctr_update) | **POST** /api/en-us/geofences/update | update CTR
[**flights_activate**](GeneralApi.md#flights_activate) | **POST** /api/en-us/flights/activate | activate flight plan
[**flights_deactivate**](GeneralApi.md#flights_deactivate) | **POST** /api/en-us/flights/deactivate | deactivate flight plan
[**flights_get**](GeneralApi.md#flights_get) | **POST** /api/en-us/flights/get | get flight plans
[**flights_register**](GeneralApi.md#flights_register) | **POST** /api/en-us/flights/register | register flight plan
[**rules_get**](GeneralApi.md#rules_get) | **GET** /api/en-us/rules/get | 
[**rules_get_time**](GeneralApi.md#rules_get_time) | **GET** /api/en-us/rules/getTime | get Time
[**telemetry**](GeneralApi.md#telemetry) | **POST** /api/en-us/uavs/telemetry | set telemetry


# **air_situation_get**
> InlineResponse2001 air_situation_get()

get air situation

This method retrieves the current air situation based on the requested parameters.<br/> The air situation includes all the CTRs, NFZs, active flight plans And UAVs.

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_response2001 import InlineResponse2001
from highlander_api.model.inline_object1 import InlineObject1
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object1 = InlineObject1(
        air_situation_type="full",
    ) # InlineObject1 |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # get air situation
        api_response = api_instance.air_situation_get(accept=accept, content_type=content_type, inline_object1=inline_object1)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->air_situation_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object1** | [**InlineObject1**](InlineObject1.md)|  | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **alerts_get**
> InlineResponse2002 alerts_get()

get alerts

This method retrieves the current alerts based on the requested parameters.<br/> The alerts are seperated into 3 categories:<br/>    1. <b>collision</b>: These alerts hold the data for any collision that might have occured between the UAVs.<br/>    2. <b>suggestion</b>: These alerts hold the data for any possible flight correction that might have occured when a collision has been detected.<br/>    3. <b>system</b>: This is the default category for any issue the system has alerted us.

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_object2 import InlineObject2
from highlander_api.model.inline_response404 import InlineResponse404
from highlander_api.model.inline_response2002 import InlineResponse2002
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object2 = InlineObject2(
        alerts_type="full",
        tail_number="tail_number_example",
    ) # InlineObject2 |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # get alerts
        api_response = api_instance.alerts_get(accept=accept, content_type=content_type, inline_object2=inline_object2)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->alerts_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object2** | [**InlineObject2**](InlineObject2.md)|  | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |
**404** | Not Found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authenticate**
> InlineResponse200 authenticate()

authenticate

This method is used to authenticate an MDOS. <br/> This method retrieves a token that must be used for any subsequent calls. <br/> The token is active for a limited amount of time and as such, needs to be refreshed before it expires to ensure a proper use of the system.

### Example


```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_object import InlineObject
from highlander_api.model.inline_response401 import InlineResponse401
from highlander_api.model.inline_response200 import InlineResponse200
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)


# Enter a context with an instance of the API client
with highlander_api.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object = InlineObject(
        user_name="user_name_example",
        password="password_example",
    ) # InlineObject |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # authenticate
        api_response = api_instance.authenticate(accept=accept, content_type=content_type, inline_object=inline_object)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->authenticate: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object** | [**InlineObject**](InlineObject.md)|  | [optional]

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ctr_add**
> InlineResponse2006 ctr_add()

add CTR

This method creates a new CTR in the system.<br/> A CTR is an area that is controlled solely by the creating MDOS and as such it won't get any collision alerts for UAVs that fly inside it.</br/> Pay attention that all other MDOSes will get the CTR as an NFZ so they will not be able to enter the CTR airspace.

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_response2006 import InlineResponse2006
from highlander_api.model.inline_response400 import InlineResponse400
from highlander_api.model.inline_object4 import InlineObject4
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object4 = InlineObject4(
        name="name_example",
        type="ctr",
        boundary=Boundary([
            {
                polygon=[
                    Position(
                        asl=3.14,
                        agl=3.14,
                        coordinates=InlineResponse20010ReasonBoundary(
                            latitude=3.14,
                            longitude=3.14,
                        ),
                    ),
                ],
            },
        ]),
        time_frame=TimeFrame(
            start_date="start_date_example",
            end_date="end_date_example",
        ),
    ) # InlineObject4 |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # add CTR
        api_response = api_instance.ctr_add(accept=accept, content_type=content_type, inline_object4=inline_object4)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->ctr_add: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object4** | [**InlineObject4**](InlineObject4.md)|  | [optional]

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |
**400** | Bad Request |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ctr_delete**
> InlineResponse2008 ctr_delete()

delete CTR

This method deletes a CTR and clears its airspace to other MDOSes

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_response2008 import InlineResponse2008
from highlander_api.model.inline_object6 import InlineObject6
from highlander_api.model.inline_response500 import InlineResponse500
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object6 = InlineObject6(
        id="id_example",
    ) # InlineObject6 |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # delete CTR
        api_response = api_instance.ctr_delete(accept=accept, content_type=content_type, inline_object6=inline_object6)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->ctr_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object6** | [**InlineObject6**](InlineObject6.md)|  | [optional]

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |
**500** | Internal Server Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ctr_get**
> InlineResponse2005 ctr_get()

get CTR

This method retireves the requested CTR based on the parameters.

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_object3 import InlineObject3
from highlander_api.model.inline_response2005 import InlineResponse2005
from highlander_api.model.inline_response4041 import InlineResponse4041
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object3 = InlineObject3(
        id="id_example",
    ) # InlineObject3 |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # get CTR
        api_response = api_instance.ctr_get(accept=accept, content_type=content_type, inline_object3=inline_object3)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->ctr_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object3** | [**InlineObject3**](InlineObject3.md)|  | [optional]

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |
**404** | Not Found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ctr_update**
> InlineResponse2007 ctr_update()

update CTR

This method updates an existing CTR

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_object5 import InlineObject5
from highlander_api.model.inline_response400 import InlineResponse400
from highlander_api.model.inline_response2007 import InlineResponse2007
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object5 = InlineObject5(
        name="name_example",
        type="type_example",
        boundary=Boundary([
            {
                polygon=[
                    Position(
                        asl=3.14,
                        agl=3.14,
                        coordinates=InlineResponse20010ReasonBoundary(
                            latitude=3.14,
                            longitude=3.14,
                        ),
                    ),
                ],
            },
        ]),
        time_frame=TimeFrame(
            start_date="start_date_example",
            end_date="end_date_example",
        ),
    ) # InlineObject5 |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # update CTR
        api_response = api_instance.ctr_update(accept=accept, content_type=content_type, inline_object5=inline_object5)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->ctr_update: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object5** | [**InlineObject5**](InlineObject5.md)|  | [optional]

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |
**400** | Bad Request |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **flights_activate**
> InlineResponse20012 flights_activate()

activate flight plan

This method mark the requested flight plan as executing, and as such it will be considered for collision detection.<br/> This method validates the flight plan and the way to the flight plan one last time before marking as executing.<br/> In case of a new suggested flight plan, it is up for the requesting MDOS to send the newly suggested plan for approval. 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_response20012 import InlineResponse20012
from highlander_api.model.inline_object10 import InlineObject10
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object10 = InlineObject10(
        fp_id="fp_id_example",
        departure_id="departure_id_example",
        way_to_flight_plan=ApiEnUsFlightsActivateWayToFlightPlan(
            tail_number="tail_number_example",
            serial_number="serial_number_example",
            type="type_example",
            name="name_example",
            radius_in_meters=3.14,
            time_frame=TimeFrame(
                start_date="start_date_example",
                end_date="end_date_example",
            ),
            checkpoints=[
                Position(
                    asl=3.14,
                    agl=3.14,
                    coordinates=InlineResponse20010ReasonBoundary(
                        latitude=3.14,
                        longitude=3.14,
                    ),
                ),
            ],
        ),
    ) # InlineObject10 |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # activate flight plan
        api_response = api_instance.flights_activate(accept=accept, content_type=content_type, inline_object10=inline_object10)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->flights_activate: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object10** | [**InlineObject10**](InlineObject10.md)|  | [optional]

### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **flights_deactivate**
> InlineResponse20013 flights_deactivate()

deactivate flight plan

This method marks a flight plan as done / cancelled. The flight plan will not be visible anymore and its airspace will be cleared.

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_response20013 import InlineResponse20013
from highlander_api.model.inline_response4002 import InlineResponse4002
from highlander_api.model.inline_object11 import InlineObject11
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object11 = InlineObject11(
        reason="reason_example",
        uav_serial_number="uav_serial_number_example",
        arrival_id="arrival_id_example",
        way_to_flight_plan=ApiEnUsFlightsDeactivateWayToFlightPlan(
            tail_number="tail_number_example",
            serial_number="serial_number_example",
            type="type_example",
            name="name_example",
            radius_in_meters=3.14,
            time_frame=TimeFrame(
                start_date="start_date_example",
                end_date="end_date_example",
            ),
            checkpoints=[
                Position(
                    asl=3.14,
                    agl=3.14,
                    coordinates=InlineResponse20010ReasonBoundary(
                        latitude=3.14,
                        longitude=3.14,
                    ),
                ),
            ],
        ),
    ) # InlineObject11 |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # deactivate flight plan
        api_response = api_instance.flights_deactivate(accept=accept, content_type=content_type, inline_object11=inline_object11)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->flights_deactivate: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object11** | [**InlineObject11**](InlineObject11.md)|  | [optional]

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |
**400** | Bad Request |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **flights_get**
> InlineResponse20010 flights_get()

get flight plans

This method retrieves a flight plan based on the requested parameters.

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_response20010 import InlineResponse20010
from highlander_api.model.inline_response4042 import InlineResponse4042
from highlander_api.model.inline_object8 import InlineObject8
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object8 = InlineObject8(
        id="id_example",
    ) # InlineObject8 |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # get flight plans
        api_response = api_instance.flights_get(accept=accept, content_type=content_type, inline_object8=inline_object8)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->flights_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object8** | [**InlineObject8**](InlineObject8.md)|  | [optional]

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |
**404** | Not Found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **flights_register**
> InlineResponse20011 flights_register()

register flight plan

This method is used to request permission to fly in a certain area.<br/> The system performs various collision detections and if everything is ok approves the flight plan. The flight plan has a 5 minute window to start executing.<br/> In case of a collision, the system tries to suggest a new flight plan. In this case, it is up to the MDOS to ask for an approval based on the suggestion. 

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_response20011 import InlineResponse20011
from highlander_api.model.inline_response4001 import InlineResponse4001
from highlander_api.model.inline_object9 import InlineObject9
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object9 = InlineObject9(
        tail_number="tail_number_example",
        serial_number="serial_number_example",
        type="type_example",
        name="name_example",
        speed=3.14,
        radius_in_meters=3.14,
        time_frame=TimeFrame(
            start_date="start_date_example",
            end_date="end_date_example",
        ),
        checkpoints=[
            Position(
                asl=3.14,
                agl=3.14,
                coordinates=InlineResponse20010ReasonBoundary(
                    latitude=3.14,
                    longitude=3.14,
                ),
            ),
        ],
        should_clear_trail=True,
    ) # InlineObject9 |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # register flight plan
        api_response = api_instance.flights_register(accept=accept, content_type=content_type, inline_object9=inline_object9)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->flights_register: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object9** | [**InlineObject9**](InlineObject9.md)|  | [optional]

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |
**400** | Bad Request |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rules_get**
> InlineResponse2004 rules_get()



This method retrieves the applicable ruleset.

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_response2004 import InlineResponse2004
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        api_response = api_instance.rules_get()
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->rules_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rules_get_time**
> InlineResponse2003 rules_get_time()

get Time

This method retrieves the current server time.<br/> An MDOS should use it whenever it needs to send a datetime field in the request.

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_response2003 import InlineResponse2003
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # get Time
        api_response = api_instance.rules_get_time()
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->rules_get_time: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **telemetry**
> InlineResponse2009 telemetry()

set telemetry

This method receives the telemetires of the active UAVs and enters them into the system. <br/> This data is used to should them in the dashboard.<br/> This data is also used for flight plan handling and collision/suggestion calculations. <br/> This method retrieves the current air situation data and any relevant alerts.

### Example

* Bearer Authentication (bearerAuth):

```python
import time
import highlander_api
from highlander_api.api import general_api
from highlander_api.model.inline_response2009 import InlineResponse2009
from highlander_api.model.inline_object7 import InlineObject7
from pprint import pprint
# Defining the host is optional and defaults to https://api.ussp.highlander.cloud
# See configuration.py for a list of all supported configuration parameters.
configuration = highlander_api.Configuration(
    host = "https://api.ussp.highlander.cloud"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = highlander_api.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with highlander_api.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = general_api.GeneralApi(api_client)
    accept = "application/json" # str | application/json (optional)
    content_type = "application/json" # str | application/json (optional)
    inline_object7 = InlineObject7(
        telemetry=[
            ApiEnUsUavsTelemetryTelemetry(
                model="model_example",
                tail_number="tail_number_example",
                serial_number="serial_number_example",
                type="mapping",
                battery=3.14,
                heading=3.14,
                takeoff_location=Position(
                    asl=3.14,
                    agl=3.14,
                    coordinates=InlineResponse20010ReasonBoundary(
                        latitude=3.14,
                        longitude=3.14,
                    ),
                ),
                is_flying=True,
                flight_status="pending",
                satellite=3.14,
                velocity=ApiEnUsUavsTelemetryVelocity(
                    x=3.14,
                    y=3.14,
                    z=3.14,
                ),
                pitch=3.14,
                roll=3.14,
                yaw=3.14,
                position=Position(
                    asl=3.14,
                    agl=3.14,
                    coordinates=InlineResponse20010ReasonBoundary(
                        latitude=3.14,
                        longitude=3.14,
                    ),
                ),
                flight_mode="manual",
                timestamp="timestamp_example",
                up_link_remote_controller_signal_quality=3.14,
                sign="sign_example",
                waypoint_index=3.14,
                pilot=ApiEnUsUavsTelemetryPilot(
                    user_email="user_email_example",
                    location=Position(
                        asl=3.14,
                        agl=3.14,
                        coordinates=InlineResponse20010ReasonBoundary(
                            latitude=3.14,
                            longitude=3.14,
                        ),
                    ),
                ),
            ),
        ],
        air_situation_type="full",
    ) # InlineObject7 |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # set telemetry
        api_response = api_instance.telemetry(accept=accept, content_type=content_type, inline_object7=inline_object7)
        pprint(api_response)
    except highlander_api.ApiException as e:
        print("Exception when calling GeneralApi->telemetry: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **str**| application/json | [optional]
 **content_type** | **str**| application/json | [optional]
 **inline_object7** | [**InlineObject7**](InlineObject7.md)|  | [optional]

### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# InlineResponse20013Reason


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time_frame** | [**TimeFrame**](TimeFrame.md) |  | 
**boundary** | [**Boundary**](Boundary.md) |  | 
**checkpoints** | [**[Position]**](Position.md) |  | 
**takeoff_location** | [**Position**](Position.md) |  | 
**status** | [**[InlineResponse20013ReasonStatus]**](InlineResponse20013ReasonStatus.md) |  | 
**center_point** | [**Position**](Position.md) |  | 
**zones** | [**[InlineResponse20010ReasonZones]**](InlineResponse20010ReasonZones.md) |  | 
**tail_number** | **str** | Tail number of the UAV. &lt;br/&gt;This is a unique value as registered and recieved by the Airspace authorities | 
**serial_number** | **str** |  | 
**type** | **str** |  | 
**name** | **str** |  | 
**radius_in_meters** | **float** |  | 
**dsos_id** | **str** |  | 
**speed** | **float** |  | 
**ussp_id** | **bool, date, datetime, dict, float, int, list, str, none_type** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



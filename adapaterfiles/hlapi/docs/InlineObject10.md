# InlineObject10


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fp_id** | **str** |  | 
**departure_id** | **str** |  | 
**way_to_flight_plan** | [**ApiEnUsFlightsActivateWayToFlightPlan**](ApiEnUsFlightsActivateWayToFlightPlan.md) |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# InlineObject11


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | **str** |  | 
**uav_serial_number** | **str** |  | 
**arrival_id** | **str** |  | 
**way_to_flight_plan** | [**ApiEnUsFlightsDeactivateWayToFlightPlan**](ApiEnUsFlightsDeactivateWayToFlightPlan.md) |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



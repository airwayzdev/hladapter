# TimeFrame


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | **str** | Should be in UTC format.&lt;br/&gt; startDate should start before endDate. | 
**end_date** | **str** | Should be in UTC format.&lt;br/&gt; endDate should end after endDate. | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



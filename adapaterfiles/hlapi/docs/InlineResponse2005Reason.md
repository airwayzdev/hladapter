# InlineResponse2005Reason


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**boundary** | [**Boundary**](Boundary.md) |  | 
**center_point** | [**Position**](Position.md) |  | 
**time_frame** | [**TimeFrame**](TimeFrame.md) |  | 
**tags** | **[str]** |  | 
**name** | **str** |  | 
**type** | **str** |  | defaults to "ctr"
**ussp_id** | **bool, date, datetime, dict, float, int, list, str, none_type** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



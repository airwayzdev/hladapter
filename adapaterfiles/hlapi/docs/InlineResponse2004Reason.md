# InlineResponse2004Reason


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**radius_in_meters** | **float** |  | 
**flight_plan_max_height** | **float** |  | 
**version_number** | **str** |  | 
**max_radius_in_meters** | **float** |  | 
**uav_altitude_separation** | **float** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# InlineObject9


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tail_number** | **str** |  | 
**serial_number** | **str** |  | 
**type** | **str** |  | 
**name** | **str** |  | 
**speed** | **float** |  | 
**radius_in_meters** | **float** |  | 
**time_frame** | [**TimeFrame**](TimeFrame.md) |  | 
**checkpoints** | [**[Position]**](Position.md) |  | 
**should_clear_trail** | **bool** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



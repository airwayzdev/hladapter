# InlineResponse20010Reason


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**time_frame** | [**TimeFrame**](TimeFrame.md) |  | 
**boundary** | [**Boundary**](Boundary.md) |  | 
**radius_in_meters** | **float** |  | 
**speed** | **float** |  | 
**tail_number** | **str** | Tail number of the UAV. &lt;br/&gt;This is a unique value as registered and recieved by the Airspace authorities | 
**should_clear_trail** | **bool** |  | 
**status** | [**[InlineResponse20010ReasonStatus]**](InlineResponse20010ReasonStatus.md) |  | 
**serial_number** | **str** |  | 
**zones** | [**[InlineResponse20010ReasonZones]**](InlineResponse20010ReasonZones.md) |  | 
**name** | **str** |  | 
**sub_fp_ids** | **[{str: (bool, date, datetime, dict, float, int, list, str, none_type)}]** |  | 
**dsos_id** | **str** |  | 
**center_point** | [**Position**](Position.md) |  | 
**takeoff_location** | [**Position**](Position.md) |  | 
**type** | **str** |  | 
**connected_geofences** | **[str]** |  | 
**checkpoints** | [**[Position]**](Position.md) |  | 
**ussp_id** | **str** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



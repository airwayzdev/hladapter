# InlineResponse2009Reason


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**air_situation** | [**InlineResponse2009ReasonAirSituation**](InlineResponse2009ReasonAirSituation.md) |  | 
**alerts** | [**InlineResponse2009ReasonAlerts**](InlineResponse2009ReasonAlerts.md) |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



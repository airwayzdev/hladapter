# InlineResponse2009ReasonAirSituation


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uavs** | **[{str: (bool, date, datetime, dict, float, int, list, str, none_type)}]** |  | 
**dynamic_nfz** | **[{str: (bool, date, datetime, dict, float, int, list, str, none_type)}]** |  | 
**flight_plan** | **[{str: (bool, date, datetime, dict, float, int, list, str, none_type)}]** |  | 
**ctr** | [**[InlineResponse2009ReasonAirSituationCtr]**](InlineResponse2009ReasonAirSituationCtr.md) |  | 
**container** | [**[InlineResponse2009ReasonAirSituationContainer]**](InlineResponse2009ReasonAirSituationContainer.md) |  | 
**bridge** | [**[InlineResponse2009ReasonAirSituationBridge]**](InlineResponse2009ReasonAirSituationBridge.md) |  | 
**nfz** | [**[InlineResponse2009ReasonAirSituationNfz]**](InlineResponse2009ReasonAirSituationNfz.md) |  | 
**static_air_space_version_number** | **str** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



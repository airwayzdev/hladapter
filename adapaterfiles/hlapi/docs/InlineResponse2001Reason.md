# InlineResponse2001Reason


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uavs** | **[{str: (bool, date, datetime, dict, float, int, list, str, none_type)}]** |  | 
**dynamic_nfz** | **[{str: (bool, date, datetime, dict, float, int, list, str, none_type)}]** |  | 
**flight_plan** | **[{str: (bool, date, datetime, dict, float, int, list, str, none_type)}]** |  | 
**ctr** | [**[InlineResponse2001ReasonCtr]**](InlineResponse2001ReasonCtr.md) |  | 
**container** | [**[InlineResponse2001ReasonContainer]**](InlineResponse2001ReasonContainer.md) |  | 
**bridge** | [**[InlineResponse2001ReasonBridge]**](InlineResponse2001ReasonBridge.md) |  | 
**nfz** | [**[InlineResponse2001ReasonNfz]**](InlineResponse2001ReasonNfz.md) |  | 
**static_air_space_version_number** | **str** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



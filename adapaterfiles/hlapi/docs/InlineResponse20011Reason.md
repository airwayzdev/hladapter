# InlineResponse20011Reason


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**status** | [**[InlineResponse20010ReasonStatus]**](InlineResponse20010ReasonStatus.md) |  | 
**center_point** | [**Position**](Position.md) |  | 
**time_frame** | [**TimeFrame**](TimeFrame.md) |  | 
**checkpoints** | [**[Position]**](Position.md) |  | 
**connected_geofences** | **[str]** |  | 
**ussp_id** | **str** |  | 
**zones** | [**[InlineResponse20010ReasonZones]**](InlineResponse20010ReasonZones.md) |  | 
**takeoff_location** | [**Position**](Position.md) |  | 
**radius_in_meters** | **float** |  | 
**tail_number** | **str** | Tail number of the UAV. &lt;br/&gt;This is a unique value as registered and recieved by the Airspace authorities | 
**boundary** | [**Boundary**](Boundary.md) |  | 
**type** | **str** |  | 
**serial_number** | **str** |  | 
**name** | **str** |  | 
**sub_fp_ids** | **[{str: (bool, date, datetime, dict, float, int, list, str, none_type)}]** |  | 
**dsos_id** | **str** |  | 
**speed** | **float** |  | 
**should_clear_trail** | **bool** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



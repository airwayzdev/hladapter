# InlineResponse20012Reason


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**tail_number** | **str** | Tail number of the UAV. &lt;br/&gt;This is a unique value as registered and recieved by the Airspace authorities | 
**sub_fp_ids** | **[{str: (bool, date, datetime, dict, float, int, list, str, none_type)}]** |  | 
**name** | **str** |  | 
**takeoff_location** | [**Position**](Position.md) |  | 
**center_point** | [**Position**](Position.md) |  | 
**dsos_id** | **str** |  | 
**boundary** | [**Boundary**](Boundary.md) |  | 
**type** | **str** |  | 
**zones** | [**[InlineResponse20010ReasonZones]**](InlineResponse20010ReasonZones.md) |  | 
**checkpoints** | [**[Position]**](Position.md) |  | 
**ussp_id** | **str** |  | 
**serial_number** | **str** |  | 
**way_to_flight_plan_id** | **str** |  | 
**speed** | **float** |  | 
**should_clear_trail** | **bool** |  | 
**status** | [**[InlineResponse20012ReasonStatus]**](InlineResponse20012ReasonStatus.md) |  | 
**connected_geofences** | **[str]** |  | 
**radius_in_meters** | **float** |  | 
**time_frame** | [**TimeFrame**](TimeFrame.md) |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



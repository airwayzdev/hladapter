# InlineResponse4002Bridge


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**boundary** | [**Boundary**](Boundary.md) |  | 
**ussp_id** | **str** |  | 
**id** | **str** |  | 
**bridge_id** | **str** |  | 
**uav_serial_number** | **str** |  | 
**type** | **str** |  | 
**created_at** | **str** |  | 
**tail_number** | **str** | Tail number of the UAV. &lt;br/&gt;This is a unique value as registered and recieved by the Airspace authorities | 
**bridge_from** | **str** |  | 
**name** | **str** |  | 
**bridge_to** | **str** |  | 
**after_validation** | **bool** |  | 
**center_point** | [**Position**](Position.md) |  | [optional] 
**time_frame** | [**TimeFrame**](TimeFrame.md) |  | [optional] 
**checkpoints** | [**[Position]**](Position.md) |  | [optional] 
**bridges_ids** | [**InlineResponse4002BridgesIds**](InlineResponse4002BridgesIds.md) |  | [optional] 
**tags** | **[str]** |  | [optional] 
**dsos_ids** | **[str]** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



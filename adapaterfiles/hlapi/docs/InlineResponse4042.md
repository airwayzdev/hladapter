# InlineResponse4042


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_message** | **str** |  | 
**trace_id** | **str** |  | 
**errors** | **[bool, date, datetime, dict, float, int, list, str, none_type]** |  | 
**type** | **float** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



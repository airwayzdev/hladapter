# InlineResponse2001ReasonBridge


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bridge_id** | **str** |  | 
**id** | **str** |  | 
**name** | **str** |  | 
**created_at** | **str** |  | 
**bridge_from** | **str** |  | 
**type** | **str** |  | 
**bridge_to** | **str** |  | 
**center_point** | [**Position**](Position.md) |  | [optional] 
**boundary** | [**Boundary**](Boundary.md) |  | [optional] 
**time_frame** | [**TimeFrame**](TimeFrame.md) |  | [optional] 
**checkpoints** | [**[Position]**](Position.md) |  | [optional] 
**bridges_ids** | [**InlineResponse2001ReasonBridgesIds**](InlineResponse2001ReasonBridgesIds.md) |  | [optional] 
**tags** | **[str]** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



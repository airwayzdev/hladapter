# InlineResponse4002BridgesIds


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**k_y_eru_ptzrmvk_mk_f6p6l_r** | **str** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



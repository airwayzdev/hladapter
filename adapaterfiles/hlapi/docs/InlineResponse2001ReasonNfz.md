# InlineResponse2001ReasonNfz


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**drawing_type** | **str** |  | 
**altitude** | **float** |  | 
**name** | **str** |  | 
**center_point** | [**Position**](Position.md) |  | [optional] 
**boundary** | [**Boundary**](Boundary.md) |  | [optional] 
**time_frame** | [**TimeFrame**](TimeFrame.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



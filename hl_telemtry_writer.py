
import signal,os
import time
import sys
import signal,os

import asyncio
from threading import Lock
from json import dumps
import traceback
from pathlib import Path
from logging.handlers import RotatingFileHandler
from awz_hl_api import uavs_telemtry
from models.alert import alert
from models.hl_global import AIR_SITUATION_TYPE, flightplan
from adapter_logger import adapter_logging




def ThreadMain(lock1, shared_data):
    # setup_logger()
    _token = shared_data.get("token", "")
    log_tel = adapter_logging("telemtry")
    log_alert = adapter_logging("alert")
    
    try:
        while True:
            
            # Read air picture reported from airwayz
            lock1.acquire()
            airwayz_ap_list = shared_data.get("AIRWAYZ_AP", []).copy()
            flightplan_awz_drones = shared_data.get("AIRWAYZ_FPID", []).copy()
            lock1.release()
            alerts = {}

            if len(airwayz_ap_list) > 0:
                # Preparing data in the apropriate format
                drones_list = []
                for item in airwayz_ap_list:
                    item.hl_fpid = flightplan_awz_drones.get(item.hl_id, None)
                    sim_item = item.get_hl_uav_telemtry()
                    if sim_item is not None:
                        drones_list.append(sim_item)
                
                # log_data(f'SENDING TO HIGHLANDER:\n{drones_list}\n')
                telemtry_res = uavs_telemtry(_token, drones_list, AIR_SITUATION_TYPE.NONE.value)
                log_tel.log_data(drones_list)
                log_tel.log_data(telemtry_res)
                try:
                    
                    if "reason" in telemtry_res and "alerts" in telemtry_res["reason"]:
                        alert_dict = telemtry_res["reason"]["alerts"]

                        if "suggestion" in alert_dict or "collision" in alert_dict:
                            if len(alert_dict["suggestion"]) > 0 or len(alert_dict["collision"]) > 0 :
                                log_alert.log_data("Real time Alert resived from highlander")
                                log_alert.log_data(alert_dict)
                            sugg_list = []
                            if "suggestion" in alert_dict:
                                sugg_list.extend(alert_dict["suggestion"])
                            elif "collision" in alert_dict:
                                sugg_list.extend(alert_dict["collision"])

                            for item in sugg_list:
                                al = alert().init_from_hl_uav_alert(item)
                                alerts[al.name] = al


                    lock1.acquire()
                    shared_data["ALERT"] = alerts
                    
                    lock1.release()
                except Exception as ex:
                    log_alert.log_data(ex)
                    log_alert.log_data(traceback.format_exc())


                if telemtry_res is not None and isinstance(telemtry_res, dict) and telemtry_res.get("type", 400) != 200:
                    log_alert.log_data("ERROR in sending telemtry")
                    log_alert.log_data(telemtry_res)
                elif isinstance(telemtry_res, str):
                    log_alert.log_data(telemtry_res)

            time.sleep(1.0)
                
            
    except Exception as ex:
        log_alert.log_data(ex)
        log_alert.log_data(traceback.format_exc())




# def setup_logger():
#     return
#     currPath = str(Path(__file__).parent.absolute())
#     logfilename = currPath + "/logs/highlander_telemtry_writer.log"
#     global log
#     log = logging.getLogger("Writer Log")
#     formatter = logging.Formatter('%(asctime)s %(levelname)s: \n%(message)s\n --- End off log ---- \n')
#     log.setLevel(logging.INFO)
#     handler = RotatingFileHandler(logfilename, maxBytes=50000000, backupCount=3)
#     handler.setFormatter(formatter)
#     log.addHandler(handler)



# def log_data(data):
#     # log = logging.getLogger("Writer Log")
#     # log.info(data)
#     print(data)



if __name__ == '__main__':
    dic = {"SDf": 2}
    print(len(dic))
    pass

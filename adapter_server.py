
import json
from http.server import BaseHTTPRequestHandler, HTTPServer, ThreadingHTTPServer
from datetime import datetime, timedelta
from operator import imod
from threading import Lock
from enum import Enum
from random import seed
from random import random
from awz_hl_api import register_flights, activate_flights, deactivate_flights, uavs_telemtry
from models.hl_global import UTC_FORMAT, hl_position
from models.utils import calculate_flightplan_duration_and_max_height_and_speed as get_duration_and_height
from models.drone_telemetry import IHReqFlightPlan, droneTelemetry
from adapter_logger import adapter_logging
from models.alert import alert

server_loger = adapter_logging("server_logger")

FLIGHT_PLAN_LAND_REPORT = {} # true -> didnt send, false send.
# if asking for take of set it to true, else set it to false
FLIGHT_PLANS = {}
class endpoints(Enum):
  register = "/register"
  getFlightPlan = "/getFlightPlanApproval"
  getFinalFlightPlan = "/getFinalFlightPlanApproval"
  cancelFlightPlan = "/cancelFlightPlan"
  syncUssData = "/syncUssData"


class myHandler(BaseHTTPRequestHandler):

    def __init__(self, request, client_address, server):
        
        self.lock = LOCK
        self.shared_data = SHARED
        self.token = SHARED.get("token", "")
        
        super().__init__(request, client_address, server)
        if self.path != "/syncUssData":
            server_loger.log_data(f"__init__ {self.path}")
        
        


    def _set_response(self, body_dic={}, code=200, from_msg=""):
        try:
            self.send_response(code)
            self.send_header('Content-type', 'application/json')
            msg = json.dumps(body_dic).encode('utf-8')
            
            # server_loger.log_data(f"Response: \n {msg}")
            self.send_header("Content-length", len(msg))
            self.end_headers()
            self.wfile.write(msg)
        except Exception as e:
            
            server_loger.log_data(e)

    def _get_syncUssData_response(self, data_input):
        dic = {"usses": {}, "nfzs":{}}
        self.lock.acquire()
        drones_list = self.shared_data.get("HL_AP", []).copy()
        nfzs_list = self.shared_data.get("NFZ", []).copy()
        corridor_list = self.shared_data.get("CORRIDOR", []).copy()
        alert_dict = self.shared_data.get("ALERT", {}).copy()

        self.lock.release()
        
        org_dict = {}
        for drone in drones_list:
            # if drone.dsosId != airwayz_dsosid:
            d_dict = drone.get_awz_uav_telemtry()
            # if drone.hl_id in alert_dict:
            #     drone_alerts = alert_dict[drone.hl_id].get_awz_sync_alert()
            #     d_dict["data"]["alerts"] = drone_alerts

            if drone.dsosId not in org_dict:
                org_dict[drone.dsosId] = {"entities" : {}, "info": {"last_update_time": "2021-08-25T14:50:08.941Z", "state": "active"}}

        
            org_dict[drone.dsosId]["entities"][drone.hl_id] = d_dict

        # this line is for supporting alerts to our uss
        org_dict["3421399088099328"] = data_input["entities"]
        for drona_name, _ in org_dict["3421399088099328"].items():
            if drona_name in alert_dict:
                drone_alerts = alert_dict[drona_name].get_awz_sync_alert() 
                org_dict["3421399088099328"][drona_name]["data"]["alerts"]= drone_alerts
        
        nfz_dict = {}
        corridor_dict = {}
        for nfz in nfzs_list:
            nfz_dict[nfz["id"]] = nfz

        for corr in corridor_list:
            corridor_dict[corr["id"]] = corr

        dic["usses"] = org_dict
        dic["nfzs"] = nfz_dict
        dic["corridor"] = corridor_dict

        return dic

    def _save_drones_in_shared_data(self, data):
        
        tmp_drone_list = []
        for drona_name, drone_data in data["entities"].items():
            tmp_obj = droneTelemetry().init_from_awz(awz_dsos_id_org_id="airwayz", key_drone_name=drona_name, awz_obj=drone_data)
            tmp_drone_list.append(tmp_obj)

        self.lock.acquire()
        self.shared_data["AIRWAYZ_AP"] = tmp_drone_list
        self.lock.release()

    def log_message(self, format, *args):
        if self.path != "/syncUssData":
            server_loger.log_data(f"log_message {self.path}")
        


    def do_POST(self):
        try:
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length).decode('utf-8')
            request_body_dict = json.loads(post_data)
            
            if self.path == endpoints.register.value:
              #*****************************************
              #***************LOG IN********************
              #*****************************************

                server_loger.log_data("Requesting for Register - Login (post)")
                res_body = {"token": self.token,
                            "time": datetime.now().strftime(UTC_FORMAT),
                            "expire": (datetime.now() + timedelta(days=1)).strftime(UTC_FORMAT)}
                self._set_response(res_body)

            elif self.path == endpoints.getFlightPlan.value:  # submitflightplan

              #*****************************************
              #***********FLIGHT APPROVAL***************
              #*****************************************
              """
              gets a route and a drone name/id
              need to conver the rout to routOBJ and gets its nfz
              after that need to build flight submit obj and send it.
              """
            
              server_loger.log_data(f"Registering Flight Plan (getFlightPlan) {datetime.utcnow()}")
              drone_name = request_body_dict["pid"]
              path = request_body_dict["path"]
              takeofalt= int(request_body_dict.get("takeoffAlt", 0))
              _seconds, _, speed_a = get_duration_and_height(path, takeofalt)
            
              response_dic = {"request_state": "approved", "planId": "default_FPID", "path": path}
              flight_plan_init_dict = {}
              flight_plan_init_dict["name"] = drone_name
              flight_plan_init_dict["radiusInMeters"] = 30.0
              flight_plan_init_dict["tailNumber"] = drone_name
              flight_plan_init_dict["serialNumber"] = drone_name
              hl_path = []
              for poi in path:
                  lat = poi["latitude"]
                  lon = poi["longitude"]
                  asl = poi["altitude"]
                #   agl = asl - takeofalt
                  agl = 20
                  hl_path.append(hl_position(lat, lon, agl, asl).to_dict())
              flight_plan_init_dict["checkpoints"] = hl_path
              flight_plan_init_dict["duration_in_second"] = _seconds
              flight_plan_init_dict["speed"] = speed_a
              ReqFlightPlan = IHReqFlightPlan(init_dict=flight_plan_init_dict)
              
              register_flights_response = register_flights(token=self.token, reqFlightPlan=ReqFlightPlan)
              if isinstance(register_flights_response, dict):
                req_status = register_flights_response.get("type", 400)
                if  req_status == 200:

                    flightplan_id = register_flights_response["reason"]["id"]
                    FLIGHT_PLANS[flightplan_id] = path
                    self.lock.acquire()
                    self.shared_data["AIRWAYZ_FPID"][ReqFlightPlan.name] = flightplan_id
                    self.lock.release()

                    response_dic["planId"] = flightplan_id

                else: # it has some error
                    # print(f"flight plan register has been failed!: \n {register_flights_response}" )
                    server_loger.log_data(f"flight plan register has been failed!: \n {register_flights_response}")
                    al = alert().init_from_hl_alert(register_flights_response)

                    response_dic["request_state"] = al.code
                    response_dic["planId"] = None
                    response_dic["path"] = al.sugpath
                    req_status = 200
                    # print(response_dic)
                    server_loger.log_data(response_dic)
              else:
                #   print()
                  server_loger.log_data(f"Error in highlander server: \n {register_flights_response}")
                  response_dic = {}
                  req_status = 400


                  
              self._set_response(response_dic, req_status)

            # check flight validity
            elif self.path == endpoints.getFinalFlightPlan.value:
              """
              *****************************************
              ***********FINAL FLIGHT APPROVAL*********
              *****************************************
              """
            #   print(f"Activating Flight Plan (getFinalFlightPlan) {datetime.utcnow()}")
              server_loger.log_data(f"Activating Flight Plan (getFinalFlightPlan) {datetime.utcnow()}")

              
              fpId = request_body_dict.get("planId")
              ctr_id = "K2zv9B2QIU3MO8h9T5Yw"
              activate_flights_response = activate_flights(token=self.token, fp_id=fpId, dep_id=ctr_id)
              path = FLIGHT_PLANS[fpId]
              response_body_dict = {"request_state": "approved", "planId": fpId, "path": path}
              status_code = activate_flights_response.get("type", 400)

              if status_code != 200:
                  status_error = activate_flights_response.get("statusMessage", "Ultra Error")
                  trace_id = activate_flights_response.get("traceId", "no trace were given")
                #   print()
                  server_loger.log_data(f"Rejected by Hilander: Error Status: {status_error} \n TraceId: {trace_id}")
                  al = alert().init_from_hl_alert(activate_flights_response)
                  response_body_dict["request_state"] = al.code
                  response_body_dict["path"] = al.sugpath
                  req_status = 200
                  
              else:
                #   print("FlightPlan Activated!")
                  server_loger.log_data(f"FlightPlan Activated!")
                  
              self._set_response(response_body_dict, code=status_code)

              
              
            elif self.path == endpoints.cancelFlightPlan.value:  # cancel flight plans
                """
                *****************************************
                ************CANCEL FLIGHT PLAN***********
                *****************************************
                """
                fp = request_body_dict.get("planId", "default")
                drone_id = request_body_dict.get("pid", "no found")
                server_loger.log_data(f"Cancel Flight plan for drone {drone_id}")
                res = deactivate_flights(token=self.token, fp_id=fp, drone_id=drone_id)

                res_body = {"request_state": "canceled"}
                if res.get("type", 400) == 200:
                    server_loger.log_data(f"Flight canceld succssfully")
                    self._set_response(res_body)
                else:
                    
                    server_loger.log_data(f"ERROR in cancaling flight plan: {res_body}" )
                    self._set_response(res_body, code=400)

            elif self.path == endpoints.syncUssData.value:
                #*****************************************
                #************SYNC USS DATA****************
                #*****************************************
                self._save_drones_in_shared_data(request_body_dict)
                response_body_dict = self._get_syncUssData_response(request_body_dict)  
                self._set_response(response_body_dict)
            else:
                server_loger.log_data(f"wrong path: {self.path} sending 404 not found")
                self._set_response(code=404)
            

        except Exception as e:
            server_loger.log_data(f"{e}")
            
            self._set_response(code=400)


def ThreadMain(lock = None, shared_data = None):

    global LOCK
    global SHARED
    global MAX_ALT
    global DSOSID
    global TOKEN

    LOCK = lock
    SHARED = shared_data
    MAX_ALT = 300 #TODO should get it from get_policy
    DSOSID = shared_data.get("DSOSID", "user01")
    TOKEN = shared_data.get("token", "bla")


    try:
        SERVER_HOST = ''
        SERVER_PORT = 9998 # MEIR! set port to proper value
        server = ThreadingHTTPServer((SERVER_HOST, SERVER_PORT), myHandler)
        print(f'Server ready on port: {SERVER_PORT} Adapter version {0.1}')
        
        
        server.serve_forever()
    except Exception as e:
        print(e)
        

# can not run as main:
if __name__ == "__main__":
    shared_data = {"SIMPLEX_AP": [], "NFZ": [], "AIRWAYZ_AP": [], "token": ""}
    ThreadMain(lock=Lock(), shared_data={})

    
else:
    global LOCK
    global SHARED


import time
from threading import Lock
from json import loads
import traceback

from numpy import isin
from models.nfz import nfz as nfz_class
from models.drone_telemetry import droneTelemetry
from datetime import datetime, timedelta
from awz_hl_api import get_air_situation
from models.hl_global import AIR_SITUATION_TYPE
# from adapter_logger import air_situation
from adapter_logger import adapter_logging


nfz_log_error = adapter_logging("nfz_log_error")
nfz_log = adapter_logging("nfz_log")

def ThreadMain(lock1, shared_data):
    _token = shared_data.get("token", "")
    option = AIR_SITUATION_TYPE.DYNAMIC.value
    try:
        while True:
            lock1.acquire()
            curr_nfz_version = shared_data.get("nfz_version", "")
            lock1.release()
            if curr_nfz_version == "":
                option = AIR_SITUATION_TYPE.STATIC.value

            _air_pic = get_air_situation(token=_token, opt=option)
            nfz_log.log_data(_air_pic)

            if _air_pic is not None and isinstance(_air_pic, dict) and _air_pic.get("type", 400) != 200:
                nfz_log_error.log_data("Error int get_air_situation, \n", _air_pic)
                continue
            elif isinstance(_air_pic, str) :
                nfz_log_error.log_data("Error int get_air_situation, \n", _air_pic)
                continue

            air_pic = _air_pic.get("reason", {})
            bridge = air_pic.get("bridge", None)
            container = air_pic.get("container", None)
            nfz = air_pic.get("nfz", None)
            ctr = air_pic.get("ctr", None)
            uavs = air_pic.get("uavs", None)
            dynamic_nfz = air_pic.get("dynamic_nfz", None)
            flight_plan = air_pic.get("flight_plan", None)
            staticAirSpaceVersionNumber = air_pic.get("staticAirSpaceVersionNumber", "")

            # if it already a static req has been made next time it is not nesseary
            if option == AIR_SITUATION_TYPE.STATIC.value:
                option = AIR_SITUATION_TYPE.DYNAMIC.value

            if staticAirSpaceVersionNumber != curr_nfz_version:
                option = AIR_SITUATION_TYPE.STATIC.value

            
            # parse bridge ?
            if bridge is not None:
                pass
            
            container_package = []
            if container is not None:
                for dict_item in container:
                    some_dsos_id = dict_item.get("container_dsos_id", "NA")
                    awz_container = nfz_class().init_from_hl_container(dict_item, some_dsos_id).get_awz_dict()
                    container_package.append(awz_container)

            # parse nfz
            nfz_package = []
            if nfz is not None:
                for dict_item in nfz:
                    awz_nfz = nfz_class().init_from_hl_nfz(dict_item).get_awz_dict()
                    nfz_package.append(awz_nfz)

            # parse ctr 
            if ctr is not None:
                for dict_item in ctr:
                    some_dsos_id = dict_item.get("container_dsos_id", "NA")
                    awz_ctr = nfz_class().init_from_hl_ctr(dict_item, some_dsos_id).get_awz_dict()
                    nfz_package.append(awz_ctr)

            # parse uavs
            uavs_package = []
            if uavs is not None:
                for dict_item in uavs:
                    uav_obj = droneTelemetry().init_from_hl_uav_telemtry(dict_item)
                    uavs_package.append(uav_obj)
                
            
            # parse dynamic nfz
            dynamic_nfz_package = []
            if dynamic_nfz is not None:
                for dict_item in dynamic_nfz:
                    if "isActive" in dict_item and dict_item["dict_item"] == True:
                        awz_dynamic_nfz = nfz_class().init_from_hl_dynamic_nfz(dict_item).get_awz_dict()
                        dynamic_nfz_package.append(awz_dynamic_nfz)

            # parse flight plan (corridor)
            flight_plan_package = []
            if flight_plan is not None:
                for dict_item in flight_plan:
                    # some_dsos_id = dict_item.get("container_dsos_id", "NA")
                    drone_id = dict_item.get("name", "NA")
                    awz_corridor = nfz_class().init_from_hl_corridor(dict_item, drone_id).get_awz_dict()
                    flight_plan_package.append(awz_corridor)
                

        
            lock1.acquire()
            shared_data["nfz_version"] = staticAirSpaceVersionNumber
            shared_data["CORRIDOR"] = flight_plan_package#awz_corridor_list
            if nfz is not None:
                 shared_data["NFZ"] = nfz_package#
                #  air_situation.log_data(nfz_package)
                 

            if len(dynamic_nfz_package) > 0:
                shared_data["NFZ"].extend(dynamic_nfz_package)

            # if its empty delete it
            shared_data["HL_AP"] = uavs_package

            lock1.release()

            time.sleep(1.0)


    except Exception as ex:
        nfz_log.log_data(ex)
        nfz_log.log_data(traceback.format_exc())



if __name__ == '__main__':
    shared_data = {"HL_AP": [], "NFZ": [], "AIRWAYZ_AP": [], "token": "", "nfz_version": ""}
    shared_data["USS_ID"] = "AZ" 
    shared_data["TIME_DIFFRENCE"] = 0
    shared_data["CONTAINER"] = []
    shared_data["CORRIDOR"] = []
    ThreadMain(Lock(), shared_data)